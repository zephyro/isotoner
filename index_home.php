<!doctype html>
<html class="no-js" lang="">

    <!-- Head -->
    <?php include('inc/head.inc.php') ?>
    <!-- -->

    <body>

        <div class="page">

            <!-- Header -->
            <?php include('inc/header.inc.php') ?>
            <!-- -->

            <!-- Navigation -->
            <?php include('inc/nav.inc.php') ?>
            <!-- -->

            <div class="main_slider">
                <div class="main_slider__container swiper-container">
                    <div class="swiper-wrapper">
                        <div class="swiper-slide">
                            <div class="main_slider__item">
                                <div class="main_slider__mobile" style="background-image: url('images/slider/slide__01_mobile.jpg')"></div>
                                <div class="main_slider__desktop" style="background-image: url('images/slider/slide__01_desktop.jpg')"></div>
                                <div class="main_slider__content">
                                    <a href="#">Женщины</a>
                                    <a href="#">Мужчины</a>
                                    <a href="#">Дети</a>
                                </div>
                            </div>
                        </div>
                        <div class="swiper-slide">
                            <div class="main_slider__item">
                                <div class="main_slider__mobile" style="background-image: url('images/slider/slide__02_mobile.jpg')"></div>
                                <div class="main_slider__desktop" style="background-image: url('images/slider/slide__02_desktop.jpg')"></div>
                                <div class="main_slider__content">
                                    <a href="#">Женщины</a>
                                    <a href="#">Мужчины</a>
                                    <a href="#">Дети</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Add Pagination -->
                    <div class="swiper-pagination"></div>
                </div>
            </div>

            <div class="push">
                <div class="container">
                    <ul class="push__row">
                        <li>
                            <a href="#">
                                <img src="images/push__01.jpg" class="img-fluid" alt="">
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <img src="images/push__02.jpg" class="img-fluid" alt="">
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <img src="images/push__03.jpg" class="img-fluid" alt="">
                            </a>
                        </li>
                    </ul>
                </div>
            </div>

            <section class="main">
                <div class="container">

                    <div class="homepage_title"><span>Discover this season</span></div>

                    <div class="showcase">

                        <div class="showcase__item">
                            <a class="goods" href="#">
                                <div class="goods__image">
                                    <img src="images/goods__01.jpg" class="img-fluid" alt="">
                                </div>
                                <h3 class="goods__name">Women's topstitched ballet flat slippers</h3>
                                <div class="goods__subtitle">Fleece - ERGONOMIC ZEN flex® sole</div>
                                <div class="goods__price"><span>€24.99</span></div>
                                <div class="goods__footer">
                                    <div class="goods__btn"><span>See more</span><i></i></div>
                                </div>
                            </a>
                        </div>

                        <div class="showcase__item">
                            <a class="goods" href="#">
                                <div class="goods__image">
                                    <img src="images/goods__02.jpg" class="img-fluid" alt="">
                                </div>
                                <h3 class="goods__name">Men's classic mule slippers</h3>
                                <div class="goods__subtitle">Velvet - ERGONOMIC sole</div>
                                <div class="goods__price"><span>€23.99</span></div>
                                <div class="goods__footer">
                                    <div class="goods__btn"><span>See more</span><i></i></div>
                                </div>
                            </a>
                        </div>

                        <div class="showcase__item">
                            <a class="goods" href="#">
                                <div class="goods__image">
                                    <img src="images/goods__03.jpg" class="img-fluid" alt="">
                                </div>
                                <h3 class="goods__name">Ultra Slim umbrella</h3>
                                <div class="goods__subtitle">Automatic open/close-Technology X-TRA  SEC - ULTRA WATER-REPELLENT</div>
                                <div class="goods__price"><span>€21.99</span></div>
                                <div class="goods__footer">
                                    <div class="goods__btn"><span>See more</span><i></i></div>
                                </div>
                            </a>
                        </div>

                        <div class="showcase__item">
                            <a class="goods" href="#">
                                <div class="goods__image">
                                    <img src="images/goods__04.jpg" class="img-fluid" alt="">
                                </div>
                                <h3 class="goods__name">Women's ballet flats with shoelace knot</h3>
                                <div class="goods__subtitle">Shock-absorbing insole</div>
                                <div class="goods__price"><span>€28.99</span></div>
                                <div class="goods__footer">
                                    <div class="goods__btn"><span>See more</span><i></i></div>
                                </div>
                            </a>
                        </div>

                    </div>

                </div>
            </section>

            <div class="innovation">
                <div class="container">
                    <div class="innovation__row">
                        <a class="innovation__item" href="#">
                            <img src="images/push_Innovation.JPG" class="img-fluid" alt="">
                        </a>
                        <a class="innovation__item" href="#">
                            <img src="images/push_Innovation2.JPG" class="img-fluid" alt="">
                        </a>
                    </div>
                </div>
            </div>

            <!-- Footer -->
            <?php include('inc/footer.inc.php') ?>
            <!-- -->

        </div>

        <!-- Scripts -->
        <?php include('inc/scripts.inc.php') ?>
        <!-- -->

    </body>
</html>
