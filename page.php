<!doctype html>
<html class="no-js" lang="">

    <!-- Head -->
    <?php include('inc/head.inc.php') ?>
    <!-- -->

    <body>

        <div class="page">

            <!-- Header -->
            <?php include('inc/header.inc.php') ?>
            <!-- -->

            <!-- Navigation -->
            <?php include('inc/nav.inc.php') ?>
            <!-- -->


            <section class="main">
                <div class="container">
                    <ul class="breadcrumb">
                        <li><a href="#">Главная</a></li>
                        <li><span></span></li>
                    </ul>

                </div>
            </section>
            <div class="fotorama"
                 data-arrows="false"
                 data-swipe="true"
                 data-nav="thumbs">
                <div class="fotorama__wrap-link" data-img="images/product/product__01.jpg"><a href="images/product/product__02.jpg" class="fancybox" data-fancybox="product1"></a></div>
                <div class="fotorama__wrap-link" data-img="images/product/product__02.jpg"><a href="images/product/product__02.jpg" class="fancybox" data-fancybox="product2"></a></div>
                <div class="fotorama__wrap-link" data-img="images/product/product__03.jpg"><a href="images/product/product__03.jpg" class="fancybox" data-fancybox="product3"></a></div>
                <div class="fotorama__wrap-link" data-img="images/product/product__04.jpg"><a href="images/product/product__04.jpg" class="fancybox" data-fancybox="product4"></a></div>
                <div class="fotorama__wrap-link" data-img="images/product/product__05.jpg"><a href="images/product/product__05.jpg" class="fancybox" data-fancybox="product5"></a></div>
            </div>

            <!-- Footer -->
            <?php include('inc/footer.inc.php') ?>
            <!-- -->

        </div>

        <!-- Scripts -->
        <?php include('inc/scripts.inc.php') ?>
        <!-- -->

    </body>
</html>
