<!doctype html>
<html class="no-js" lang="">

    <!-- Head -->
    <?php include('inc/head.inc.php') ?>
    <!-- -->

    <body>

        <div class="page">

            <!-- Header -->
            <?php include('inc/header.inc.php') ?>
            <!-- -->

            <!-- Navigation -->
            <?php include('inc/nav.inc.php') ?>
            <!-- -->


            <section class="main">
                <div class="container">
                    <ul class="breadcrumb">
                        <li><a href="#">Главная</a></li>
                        <li><span>Текстовая страница</span></li>
                    </ul>

                    <h1 class="text-center color_red">Comment bien choisir son parapluie</h1>

                    <div class="content_box">
                        <div class="row">
                            <div class="col col-xs-12 col-sm-6 col-lg-5">
                                <div class="content_box__image">
                                    <img src="images/image__page_01.png" class="img-fluid" alt="">
                                </div>
                            </div>
                            <div class="col col-xs-12 col-sm-6 col-lg-7">
                                <h3 class="text-uppercase">La taille du parapluie fermé et la légèreté :</h3>
                                <div class="content_box__text">
                                    <p>
                                        Si vous préférez un parapluie de petite taille, pratique et facile à glisser dans votre sac ou votre poche, <a href="#">Isotoner a développé</a> les gammes de parapluies SLIM et MINI.
                                        Véritable innovation Isotoner, la construction inédite des baleines formant l'armature de votre parapluie permet une miniaturisation incroyable du parapluie fermé.
                                    </p>
                                    <p>
                                        Ultra légers mais très solides, les parapluies SLIM et MINI sont une révolution !
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="content_box">
                        <div class="row">
                            <div class="col col-xs-12 col-sm-6 col-lg-5">
                                <div class="content_box__image">
                                    <img src="images/image__page_02.png" class="img-fluid" alt="">
                                </div>
                            </div>
                            <div class="col col-xs-12 col-sm-6 col-lg-7">
                                <h3 class="text-uppercase">LA SOLIDITÉ</h3>
                                <div class="content_box__text">
                                    <p>
                                        La solidité de tous les parapluies Isotoner est testée en laboratoire selon des critères exigeants. Chaque parapluie Isotoner est solide et particulièrement résistant aux grosses pluies et aux vents forts.
                                        Fort de ses acquis, Isotoner a choisi de lancer une gamme encore plus innovante : les parapluies X-TRA Solide.
                                    </p>
                                    <p>
                                        Ultra-résistants et anti-retournements grâce à leur structure en aluminium renforcé et fibre de verre, ces parapluies révolutionnaires peuvent affronter des forces de vent jusqu'à 60% supérieures à celles supportées par un parapluie standard de la concurrence.
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="content_box">
                        <div class="row">
                            <div class="col col-xs-12 col-sm-6 col-lg-5">
                                <div class="content_box__image">
                                    <img src="images/image__page_03.png" class="img-fluid" alt="">
                                </div>
                            </div>
                            <div class="col col-xs-12 col-sm-6 col-lg-7">
                                <h3 class="text-uppercase">La taille du parapluie fermé et la légèreté :</h3>
                                <div class="content_box__text">
                                    <p>
                                        Si vous souhaitez une grande taille de couverture lorsque <a href="#">le parapluie est ouvert</a>, Isotoner vous propose les parapluies cannes ainsi que la gamme X-TRA Large. Leurs larges diamètres et leurs longues cannes vous permettront d’être totalement protégé de la pluie, ils sont également idéaux pour les ballades à deux.
                                        En plus de la solidité, la gamme <a href="#">X-TRA Solide propose</a> également des modèles ayant une large couverture pour une meilleure protection contre les intempéries
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="table_title">Parapluies femme isotoner</div>

                    <div class="table_responsive mb_40">
                        <table class="table table_data text-center">
                            <tr>
                                <td class="no_border table_heading_col"><span class="table_heading_text">Classement par taille du + petit au + grand (parapluie fermé)</span></td>
                                <td class="no_border">
                                    <img src="images/table/img_01.jpg" alt="">
                                </td>
                                <td class="no_border">
                                    <img src="images/table/img_02.jpg" alt="">
                                </td>
                                <td class="no_border">
                                    <img src="images/table/img_03.jpg" alt="">
                                </td>
                                <td class="no_border">
                                    <img src="images/table/img_04.jpg" alt="">
                                </td>
                                <td class="no_border">
                                    <img src="images/table/img_05.jpg" alt="">
                                </td>
                                <td class="no_border">
                                    <img src="images/table/img_06.jpg" alt="">
                                </td>
                                <td class="no_border">
                                    <img src="images/table/img_07.jpg" alt="">
                                </td>
                                <td class="no_border">
                                    <img src="images/table/img_08.jpg" alt="">
                                </td>
                                <td class="no_border">
                                    <img src="images/table/img_09.jpg" alt="">
                                </td>
                            </tr>
                            <tr>
                                <td class="table_heading text-uppercase">RéFéRENCE</td>
                                <td class="table_heading text-uppercase">09137</td>
                                <td class="table_heading text-uppercase">09134</td>
                                <td class="table_heading text-uppercase">09145</td>
                                <td class="table_heading text-uppercase">09451</td>
                                <td class="table_heading text-uppercase">09189</td>
                                <td class="table_heading text-uppercase">09249</td>
                                <td class="table_heading text-uppercase">09406</td>
                                <td class="table_heading text-uppercase">09357</td>
                                <td class="table_heading text-uppercase">09457</td>
                            </tr>
                            <tr>
                                <td class="table_red text-uppercase">RéFéRENCE</td>
                                <td class="color_red text-uppercase">MINI</td>
                                <td class="color_red text-uppercase">MINI Plat</td>
                                <td class="color_red text-uppercase">MINI</td>
                                <td class="color_red text-uppercase">X-TRA Solide</td>
                                <td class="color_red text-uppercase">SLIM</td>
                                <td class="color_red text-uppercase">SLIM</td>
                                <td class="color_red text-uppercase">X-TRA Solide</td>
                                <td class="color_red text-uppercase">Canne <br> transparent</td>
                                <td class="color_red text-uppercase">Canne automatique</td>
                            </tr>
                            <tr>
                                <td class="table_blue text-uppercase">Type d'ouverture</td>
                                <td>Manuel</td>
                                <td>Manuel</td>
                                <td>Automatique</td>
                                <td>Automatique</td>
                                <td>Manuel</td>
                                <td>Manuel</td>
                                <td>Automatique</td>
                                <td>Manuel</td>
                                <td>Automatique</td>
                            </tr>
                            <tr>
                                <td class="table_blue text-uppercase">Type de fermeture</td>
                                <td>Manuel</td>
                                <td>Manuel</td>
                                <td>Automatique</td>
                                <td>Automatique</td>
                                <td>Manuel</td>
                                <td>Manuel</td>
                                <td>Automatique</td>
                                <td>Manuel</td>
                                <td>Manuel</td>
                            </tr>
                            <tr>
                                <td class="table_blue text-uppercase">Taille du parapluie fermé</td>
                                <td>16,5 cm</td>
                                <td>16,5 cm</td>
                                <td>21 cm</td>
                                <td>23 cm</td>
                                <td>24,6 cm</td>
                                <td>28 cm</td>
                                <td>28 cm</td>
                                <td>85 cm</td>
                                <td>90 cm</td>
                            </tr>
                            <tr>
                                <td class="table_blue text-uppercase">Taille couverture</td>
                                <td>94 cm</td>
                                <td>94 cm</td>
                                <td>98 cm</td>
                                <td>100 cm</td>
                                <td>94 cm</td>
                                <td>92 cm</td>
                                <td>97 cm</td>
                                <td>94 cm</td>
                                <td>100 cm</td>
                            </tr>
                            <tr>
                                <td class="table_blue text-uppercase">Baleines : nombre et matière</td>
                                <td>6-acier</td>
                                <td>6-acier/alu</td>
                                <td>6-acier</td>
                                <td>6-alu/fibre de verre</td>
                                <td>6-acier/alu</td>
                                <td>7-acier/alu/<br>carbone</td>
                                <td>8-alu/fibre de verre</td>
                                <td>8-fibre de verre</td>
                                <td>8-acier</td>
                            </tr>
                            <tr>
                                <td class="table_blue text-uppercase">Poids</td>
                                <td>+/-248 g</td>
                                <td>+/-219 g</td>
                                <td>+/-340 g</td>
                                <td>+/- 340 g</td>
                                <td>+/-189 g</td>
                                <td>+/-340 g</td>
                                <td>+/-329 g</td>
                                <td>+/-480 g</td>
                                <td>+/-495 g</td>
                            </tr>
                        </table>
                    </div>


                    <div class="table_title">Parapluies femme isotoner</div>

                    <div class="table_responsive mb_40">
                        <table class="table table_data text-center">
                            <tr>
                                <td class="no_border table_heading_col"><span class="table_heading_text">Classement par taille du + petit au + grand (parapluie fermé)</span></td>
                                <td class="no_border">
                                    <img src="images/table/img_01.jpg" alt="">
                                </td>
                                <td class="no_border">
                                    <img src="images/table/img_02.jpg" alt="">
                                </td>
                                <td class="no_border">
                                    <img src="images/table/img_03.jpg" alt="">
                                </td>
                                <td class="no_border">
                                    <img src="images/table/img_04.jpg" alt="">
                                </td>
                                <td class="no_border">
                                    <img src="images/table/img_05.jpg" alt="">
                                </td>
                                <td class="no_border">
                                    <img src="images/table/img_06.jpg" alt="">
                                </td>
                                <td class="no_border">
                                    <img src="images/table/img_07.jpg" alt="">
                                </td>
                                <td class="no_border">
                                    <img src="images/table/img_08.jpg" alt="">
                                </td>
                                <td class="no_border">
                                    <img src="images/table/img_09.jpg" alt="">
                                </td>
                            </tr>
                            <tr>
                                <td class="table_heading text-uppercase">RéFéRENCE</td>
                                <td class="table_heading text-uppercase">09137</td>
                                <td class="table_heading text-uppercase">09134</td>
                                <td class="table_heading text-uppercase">09145</td>
                                <td class="table_heading text-uppercase">09451</td>
                                <td class="table_heading text-uppercase">09189</td>
                                <td class="table_heading text-uppercase">09249</td>
                                <td class="table_heading text-uppercase">09406</td>
                                <td class="table_heading text-uppercase">09357</td>
                                <td class="table_heading text-uppercase">09457</td>
                            </tr>
                            <tr>
                                <td class="table_red text-uppercase">RéFéRENCE</td>
                                <td class="color_red text-uppercase">MINI</td>
                                <td class="color_red text-uppercase">MINI Plat</td>
                                <td class="color_red text-uppercase">MINI</td>
                                <td class="color_red text-uppercase">X-TRA Solide</td>
                                <td class="color_red text-uppercase">SLIM</td>
                                <td class="color_red text-uppercase">SLIM</td>
                                <td class="color_red text-uppercase">X-TRA Solide</td>
                                <td class="color_red text-uppercase">Canne <br> transparent</td>
                                <td class="color_red text-uppercase">Canne automatique</td>
                            </tr>
                            <tr>
                                <td class="table_blue text-uppercase">Type d'ouverture</td>
                                <td>Manuel</td>
                                <td>Manuel</td>
                                <td>Automatique</td>
                                <td>Automatique</td>
                                <td>Manuel</td>
                                <td>Manuel</td>
                                <td>Automatique</td>
                                <td>Manuel</td>
                                <td>Automatique</td>
                            </tr>
                            <tr>
                                <td class="table_blue text-uppercase">Type de fermeture</td>
                                <td>Manuel</td>
                                <td>Manuel</td>
                                <td>Automatique</td>
                                <td>Automatique</td>
                                <td>Manuel</td>
                                <td>Manuel</td>
                                <td>Automatique</td>
                                <td>Manuel</td>
                                <td>Manuel</td>
                            </tr>
                            <tr>
                                <td class="table_blue text-uppercase">Taille du parapluie fermé</td>
                                <td>16,5 cm</td>
                                <td>16,5 cm</td>
                                <td>21 cm</td>
                                <td>23 cm</td>
                                <td>24,6 cm</td>
                                <td>28 cm</td>
                                <td>28 cm</td>
                                <td>85 cm</td>
                                <td>90 cm</td>
                            </tr>
                            <tr>
                                <td class="table_blue text-uppercase">Taille couverture</td>
                                <td>94 cm</td>
                                <td>94 cm</td>
                                <td>98 cm</td>
                                <td>100 cm</td>
                                <td>94 cm</td>
                                <td>92 cm</td>
                                <td>97 cm</td>
                                <td>94 cm</td>
                                <td>100 cm</td>
                            </tr>
                            <tr>
                                <td class="table_blue text-uppercase">Baleines : nombre et matière</td>
                                <td>6-acier</td>
                                <td>6-acier/alu</td>
                                <td>6-acier</td>
                                <td>6-alu/fibre de verre</td>
                                <td>6-acier/alu</td>
                                <td>7-acier/alu/<br>carbone</td>
                                <td>8-alu/fibre de verre</td>
                                <td>8-fibre de verre</td>
                                <td>8-acier</td>
                            </tr>
                            <tr>
                                <td class="table_blue text-uppercase">Poids</td>
                                <td>+/-248 g</td>
                                <td>+/-219 g</td>
                                <td>+/-340 g</td>
                                <td>+/- 340 g</td>
                                <td>+/-189 g</td>
                                <td>+/-340 g</td>
                                <td>+/-329 g</td>
                                <td>+/-480 g</td>
                                <td>+/-495 g</td>
                            </tr>
                        </table>
                    </div>

                </div>
            </section>


            <!-- Footer -->
            <?php include('inc/footer.inc.php') ?>
            <!-- -->

        </div>

        <!-- Scripts -->
        <?php include('inc/scripts.inc.php') ?>
        <!-- -->

    </body>
</html>
