<!doctype html>
<html class="no-js" lang="">

    <!-- Head -->
    <?php include('inc/head.inc.php') ?>
    <!-- -->

    <body>

        <div class="page">

            <!-- Header -->
            <?php include('inc/header.inc.php') ?>
            <!-- -->

            <!-- Navigation -->
            <?php include('inc/nav.inc.php') ?>
            <!-- -->


            <section class="main">
                <div class="container">

                    <h1>Мой аккаунт</h1>

                    <div class="main_row">
                        <div class="main_sidebar">

                            <div class="account_navigation">
                                <div class="account_navigation__title"><i class="fa fa fa-user-o"></i><span>Mon compte</span></div>
                                <ul>
                                    <li class="active"><a href="#">Mes données personnelles</a></li>
                                    <li><a href="#">Mes adresses</a></li>
                                    <li><a href="#">Mes newsletters</a></li>
                                    <li><a href="#">Contact</a></li>
                                    <li><a href="#">Mes messages</a></li>
                                </ul>
                            </div>

                            <ul class="account_links">
                                <li class="item1"><a href="#">Mes points fidélité</a></li>
                                <li class="item2"><a href="#">Mes favoris</a></li>
                                <li class="item3"><a href="#">Mes commandes</a></li>
                                <li class="item4"><a href="#">Déconnexion</a></li>
                            </ul>

                        </div>
                        <div class="main_content">
                            <div class="content"></div>
                            <p>Vous êtes sur le nouveau site Isotoner. Vos points fidélité acquis sur notre ancien site ont bien été conservés. Si vous désirez consulter une facture d'une commande passée avant le 5 octobre 2014, merci de nous adresser un message via l'onglet contact de votre compte. Veuillez nous excuser pour ce désagrément.</p>

                            <div class="account_block">
                                <div class="account_title"><span>Личные данные</span></div>
                                <div class="row">
                                    <div class="col col-xs-12 col-lg-8 col-gutter-lr">

                                        <div class="row">
                                            <div class="col col-xs-12 col-md-6">
                                                <h3 class="mb_20">Контактная информация</h3>
                                                <p>M. K Andrey<br>
                                                    konyaev@mirlk.ru<br>
                                                </p>
                                                <a href="https://www.isotoner.fr/customer/account/edit/changepass/1/">Сменить пароль</a>
                                            </div>
                                            <div class="col col-xs-12 col-md-6 text-right col-gutter-lr">
                                                <a href="#" class="btn btn_blue btn_right">Изменить</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="account_block">
                                <div class="account_title"><span>Адреса</span></div>
                                <div class="row">
                                    <div class="col col-xs-12 col-md-6 col-xl-4">
                                        <h3>Адрес по умолчанию</h3>
                                        <p>Вы не установили платежный адрес по умолчанию.</p>
                                        <br/>
                                        <a href="#">Изменить адрес</a>
                                    </div>
                                    <div class="col col-xs-12 col-md-6 col-xl-4">
                                        <h3>Адрес доставки по умолчанию</h3>
                                        <p>Вы не установили платежный адрес по умолчанию.</p>
                                        <br/>
                                        <a href="#">Изменить адрес</a>
                                    </div>
                                    <div class="col col-xs-12 col-md-12 col-xl-4 text-right">
                                        <a href="#" class="btn btn_blue btn_right">управление адресами</a>
                                    </div>
                                </div>

                            </div>

                        </div>
                    </div>


                </div>
            </section>


            <!-- Footer -->
            <?php include('inc/footer.inc.php') ?>
            <!-- -->

        </div>

        <!-- Scripts -->
        <?php include('inc/scripts.inc.php') ?>
        <!-- -->

    </body>
</html>
