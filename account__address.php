<!doctype html>
<html class="no-js" lang="">

    <!-- Head -->
    <?php include('inc/head.inc.php') ?>
    <!-- -->

    <body>

        <div class="page">

            <!-- Header -->
            <?php include('inc/header.inc.php') ?>
            <!-- -->

            <!-- Navigation -->
            <?php include('inc/nav.inc.php') ?>
            <!-- -->


            <section class="main">
                <div class="container">

                    <h1>Мой аккаунт</h1>

                    <div class="main_row">
                        <div class="main_sidebar">

                            <div class="account_navigation">
                                <div class="account_navigation__title"><i class="fa fa fa-user-o"></i><span>Mon compte</span></div>
                                <ul>
                                    <li><a href="#">Mes données personnelles</a></li>
                                    <li class="active"><a href="#">Mes adresses</a></li>
                                    <li><a href="#">Mes newsletters</a></li>
                                    <li><a href="#">Contact</a></li>
                                    <li><a href="#">Mes messages</a></li>
                                </ul>
                            </div>

                            <ul class="account_links">
                                <li class="item1"><a href="#">Mes points fidélité</a></li>
                                <li class="item2"><a href="#">Mes favoris</a></li>
                                <li class="item3"><a href="#">Mes commandes</a></li>
                                <li class="item4"><a href="#">Déconnexion</a></li>
                            </ul>

                        </div>
                        <div class="main_content">

                            <div class="account_title"><span>Добавить новый адрес</span></div>

                            <form class="form">
                                <div class="row">
                                    <div class="col col-xs-12 col-md-6 col-gutter-lr">
                                        <div class="form_group">
                                            <label class="form_label">Имя <sup>*</sup></label>
                                            <input type="text" name="name" class="form_control" placeholder="">
                                        </div>
                                    </div>
                                    <div class="col col-xs-12 col-md-6 col-gutter-lr">
                                        <div class="form_group">
                                            <label class="form_label">Фамилия</label>
                                            <input type="text" name="name" class="form_control" placeholder="">
                                        </div>
                                    </div>
                                    <div class="col col-xs-12 col-gutter-lr">
                                        <div class="form_group">
                                            <label class="form_label">Адрес</label>
                                            <input type="text" name="name" class="form_control" placeholder="">
                                        </div>
                                    </div>
                                    <div class="col col-xs-12 col-gutter-lr">
                                        <div class="form_group">
                                            <label class="form_label">Адрес</label>
                                            <input type="text" name="name" class="form_control" placeholder="">
                                        </div>
                                    </div>
                                    <div class="col col-xs-12 col-md-6 col-gutter-lr">
                                        <div class="form_group">
                                            <label class="form_label">Страна <sup>*</sup></label>
                                            <select class="form_control form_select" name="name">
                                                <option value="">Россия</option>
                                                <option value="">Франция</option>
                                                <option value="">Испания</option>
                                                <option value="">Бельгия</option>
                                                <option value="">Латвия</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col col-xs-12 col-md-6 col-gutter-lr">
                                        <div class="form_group">
                                            <label class="form_label">Почтовый индекс <sup>*</sup></label>
                                            <input type="text" name="name" class="form_control" placeholder="">
                                        </div>
                                    </div>
                                    <div class="col col-xs-12 col-md-6 col-gutter-lr">
                                        <label class="form_label">Город<sup>*</sup></label>
                                        <select class="form_control form_select" name="name">
                                            <option value="">Москва</option>
                                            <option value="">Волгоград</option>
                                            <option value="">Новгород</option>
                                            <option value="">Рязань</option>
                                            <option value="">Новосибирск</option>
                                        </select>
                                    </div>
                                    <div class="col col-xs-12 col-md-6 col-gutter-lr">
                                        <div class="form_group">
                                            <label class="form_label">Телефон</label>
                                            <input type="text" name="name" class="form_control" placeholder="">
                                        </div>
                                    </div>
                                </div>
                                <div class="form_btn text-right">
                                    <button type="submit" class="btn btn_right">Сохранить</button>
                                </div>
                            </form>

                        </div>
                    </div>


                </div>
            </section>


            <!-- Footer -->
            <?php include('inc/footer.inc.php') ?>
            <!-- -->

        </div>

        <!-- Scripts -->
        <?php include('inc/scripts.inc.php') ?>
        <!-- -->

    </body>
</html>
