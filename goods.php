<!doctype html>
<html class="no-js" lang="">

    <!-- Head -->
    <?php include('inc/head.inc.php') ?>
    <!-- -->

    <body>

        <div class="page">

            <!-- Header -->
            <?php include('inc/header.inc.php') ?>
            <!-- -->

            <!-- Navigation -->
            <?php include('inc/nav.inc.php') ?>
            <!-- -->


            <section class="main">
                <div class="container">
                    <ul class="breadcrumb">
                        <li><a href="#">ACCUEIL</a></li>
                        <li><a href="#">Enfant</a></li>
                        <li><span>CHAUSSONS</span></li>
                    </ul>


                    <span class="btn_filter_mobile filter_toggle">Afficher les filtres</span>
                    <div class="main_row">
                        <div class="main_sidebar invert_xs">
                            <div class="filter">

                                <span class="filter__close filter_toggle"></span>

                                <div class="filter__wrap">

                                    <div class="filter__heading">CATÉGORIE</div>
                                    <div class="filter__group">
                                        <ul class="filter__link">
                                            <li>
                                                <label>
                                                    <input type="checkbox" name="f1" value="">
                                                    <span>Chaussons</span>
                                                </label>
                                            </li>
                                            <li>
                                                <label>
                                                    <input type="checkbox" name="f1" value="">
                                                    <span>Chaussures</span>
                                                </label>
                                            </li>
                                            <li>
                                                <label>
                                                    <input type="checkbox" name="f1" value="">
                                                    <span>Gants</span>
                                                </label>
                                            </li>
                                            <li>
                                                <label>
                                                    <input type="checkbox" name="f1" value="">
                                                    <span>Parapluies</span>
                                                </label>
                                            </li>
                                            <li>
                                                <label>
                                                    <input type="checkbox" name="f1" value="">
                                                    <span>Bonnets et Echarpes</span>
                                                </label>
                                            </li>
                                            <li>
                                                <label>
                                                    <input type="checkbox" name="f1" value="">
                                                    <span>Chaussettes anti dérapantes</span>
                                                </label>
                                            </li>
                                            <li>
                                                <label>
                                                    <input type="checkbox" name="f1" value="">
                                                    <span>Lunettes de soleil</span>
                                                </label>
                                            </li>
                                            <li>
                                                <label>
                                                    <input type="checkbox" name="f1" value="">
                                                    <span>Nouveautés</span>
                                                </label>
                                            </li>
                                            <li>
                                                <label>
                                                    <input type="checkbox" name="f1" value="">
                                                    <span>AFFIC</span>
                                                </label>
                                            </li>
                                        </ul>
                                    </div>

                                    <div class="filter__heading">AFFICHER LES FILTRES</div>

                                    <div class="filter__group">
                                        <div class="filter__title"><span>GENRE</span></div>
                                        <ul class="filter__list">
                                            <li>
                                                <label>
                                                    <input type="radio" name="f2" value="">
                                                    <span>Fille</span>
                                                </label>
                                            </li>
                                            <li>
                                                <label>
                                                    <input type="radio" name="f2" value="">
                                                    <span>Garçon</span>
                                                </label>
                                            </li>
                                        </ul>
                                    </div>

                                    <div class="filter__group">
                                        <div class="filter__title"><span>AGE</span></div>
                                        <ul class="filter__list">
                                            <li>
                                                <label>
                                                    <input type="radio" name="f3" value="">
                                                    <span>Bébé 0-3 ans</span>
                                                </label>
                                            </li>
                                            <li>
                                                <label>
                                                    <input type="radio" name="f3" value="">
                                                    <span>Enfant 4-9 ans</span>
                                                </label>
                                            </li>
                                            <li>
                                                <label>
                                                    <input type="radio" name="f3" value="">
                                                    <span>Ado 10-14 ans</span>
                                                </label>
                                            </li>
                                        </ul>
                                    </div>

                                    <div class="filter__group">
                                        <div class="filter__title"><span>TYPE</span></div>
                                        <ul class="filter__list">
                                            <li>
                                                <label>
                                                    <input type="radio" name="f4" value="">
                                                    <span>Ballerines</span>
                                                </label>
                                            </li>
                                            <li>
                                                <label>
                                                    <input type="radio" name="f4" value="">
                                                    <span>Mules</span>
                                                </label>
                                            </li>
                                            <li>
                                                <label>
                                                    <input type="radio" name="f4" value="">
                                                    <span>Bottillons</span>
                                                </label>
                                            </li>
                                            <li>
                                                <label>
                                                    <input type="radio" name="f4" value="">
                                                    <span>Mocassins</span>
                                                </label>
                                            </li>
                                            <li>
                                                <label>
                                                    <input type="radio" name="f4" value="">
                                                    <span>Charentaises</span>
                                                </label>
                                            </li>
                                            <li>
                                                <label>
                                                    <input type="radio" name="f4" value="">
                                                    <span>Autres</span>
                                                </label>
                                            </li>
                                        </ul>
                                    </div>

                                    <div class="filter__group">
                                        <div class="filter__title"><span>TAILLE</span></div>
                                        <ul class="filter__circle">
                                            <li>
                                                <label>
                                                    <input type="radio" name="f5" value="">
                                                    <span>22</span>
                                                </label>
                                            </li>
                                            <li>
                                                <label>
                                                    <input type="radio" name="f5" value="">
                                                    <span>23</span>
                                                </label>
                                            </li>
                                            <li>
                                                <label>
                                                    <input type="radio" name="f5" value="">
                                                    <span>24</span>
                                                </label>
                                            </li>
                                            <li>
                                                <label>
                                                    <input type="radio" name="f5" value="">
                                                    <span>25</span>
                                                </label>
                                            </li>
                                            <li>
                                                <label>
                                                    <input type="radio" name="f5" value="">
                                                    <span>26</span>
                                                </label>
                                            </li>
                                            <li>
                                                <label>
                                                    <input type="radio" name="f5" value="">
                                                    <span>27</span>
                                                </label>
                                            </li>
                                            <li>
                                                <label>
                                                    <input type="radio" name="f5" value="">
                                                    <span>28</span>
                                                </label>
                                            </li>
                                            <li>
                                                <label>
                                                    <input type="radio" name="f5" value="">
                                                    <span>29</span>
                                                </label>
                                            </li>
                                        </ul>
                                    </div>

                                    <div class="filter__group">
                                        <div class="filter__title"><span>COULEUR</span></div>
                                        <ul class="filter__color">
                                            <li>
                                                <label>
                                                    <input type="radio" name="f4" value="">
                                                    <span><img src="img/colors/color_01.jpg"></span>
                                                </label>
                                            </li>
                                            <li>
                                                <label>
                                                    <input type="radio" name="f4" value="">
                                                    <span><img src="img/colors/color_02.jpg"></span>
                                                </label>
                                            </li>
                                            <li>
                                                <label>
                                                    <input type="radio" name="f4" value="">
                                                    <span><img src="img/colors/color_03.jpg"></span>
                                                </label>
                                            </li>
                                            <li>
                                                <label>
                                                    <input type="radio" name="f4" value="">
                                                    <span><img src="img/colors/color_04.jpg"></span>
                                                </label>
                                            </li>
                                            <li>
                                                <label>
                                                    <input type="radio" name="f4" value="">
                                                    <span><img src="img/colors/color_05.jpg"></span>
                                                </label>
                                            </li>
                                            <li>
                                                <label>
                                                    <input type="radio" name="f4" value="">
                                                    <span><img src="img/colors/color_06.jpg"></span>
                                                </label>
                                            </li>
                                        </ul>
                                    </div>

                                </div>

                            </div>
                        </div>
                        <div class="main_content invert_xs">

                            <div class="categories_content">
                                <h1>Chaussons Enfant</h1>
                                <h2>Des chaussons pour enfants conçus pour le confort de vos enfants</h2>
                                <p>Profitez de la livraison gratuite dès 39€ d'achats sur toute la collection de chaussons enfant Isotoner qui développe à chaque saison une nouvelle collection : soucieux de leur confort, Isotoner a développé des chaussons aux semelles amortissantes pour les plus petits et des chaussons mules à semelle ergonomique pour les plus grands.</p>
                                <p>Botillons, mocassins, ballerines ou mules, les chaussons pantoufles Isotoner offre un large choix de modèles de chaussons de bébé et de chaussons enfants. Pour les bébés, les bottillons à scratch ou à zip plaisent aux enfants et aux parents. En effet, les chaussons à scratchs ou à zip ont la particularité d’avoir des fermetures très pratiques et sont faciles à enfiler pour les enfants, de plus, ils ont de jolis motifs brodés sur le dessus. Ces chaussons à motifs sont décorés d’un cœur, d’un lapin, d’un pompier ou imprimés, comme nos chaussettes antidérapantes</p>
                            </div>

                            <div class="toolbar">
                                <div class="toolbar__amount">50 article(s) trouvé(s)</div>
                                <div class="toolbar__sort">
                                    <span>Trier par :</span>
                                    <a class="active" href="#">Prix croissants</a>
                                    <a href="#">Prix décroissants</a>
                                </div>
                            </div>

                            <div class="product_grid">

                                <div class="product_grid__item">

                                    <a href="#" class="item">
                                        <div class="item__article">99440-AA1</div>
                                        <div class="item__image">
                                            <div class="item__image_base">
                                                <img src="images/bot_01.jpg" class="img-fluid" alt="">
                                            </div>
                                            <div class="item__image_hover">
                                                <img src="images/bot_02.jpg" class="img-fluid" alt="">
                                            </div>
                                        </div>
                                        <div class="item__name">
                                            <strong>Chaussons bottillons velcro fille licorne</strong>
                                            <span>Velours</span>
                                        </div>
                                        <div class="item__price">
                                            <span>14,99 €</span>
                                        </div>
                                        <div class="item__options">
                                            <div class="item__options_title">Tailles disponibles</div>
                                            <ul>
                                                <li>20</li>
                                                <li>21</li>
                                                <li>22</li>
                                                <li>23</li>
                                            </ul>
                                        </div>
                                    </a>

                                </div>
                                <div class="product_grid__item">

                                    <a href="#" class="item">
                                        <div class="item__article">99440-AA1</div>
                                        <div class="item__image">
                                            <div class="item__image_base">
                                                <img src="images/bot_01.jpg" class="img-fluid" alt="">
                                            </div>
                                            <div class="item__image_hover">
                                                <img src="images/bot_02.jpg" class="img-fluid" alt="">
                                            </div>
                                        </div>
                                        <div class="item__name">
                                            <strong>Chaussons bottillons velcro fille licorne</strong>
                                            <span>Velours</span>
                                        </div>
                                        <div class="item__price">
                                            <span>14,99 €</span>
                                        </div>
                                        <div class="item__options">
                                            <div class="item__options_title">Tailles disponibles</div>
                                            <ul>
                                                <li>20</li>
                                                <li>21</li>
                                                <li>22</li>
                                                <li>23</li>
                                            </ul>
                                        </div>
                                    </a>

                                </div>
                                <div class="product_grid__item">

                                    <a href="#" class="item">
                                        <div class="item__article">99440-AA1</div>
                                        <div class="item__image">
                                            <div class="item__image_base">
                                                <img src="images/bot_01.jpg" class="img-fluid" alt="">
                                            </div>
                                            <div class="item__image_hover">
                                                <img src="images/bot_02.jpg" class="img-fluid" alt="">
                                            </div>
                                        </div>
                                        <div class="item__name">
                                            <strong>Chaussons bottillons velcro fille licorne</strong>
                                            <span>Velours</span>
                                        </div>
                                        <div class="item__price">
                                            <span>14,99 €</span>
                                        </div>
                                        <div class="item__options">
                                            <div class="item__options_title">Tailles disponibles</div>
                                            <ul>
                                                <li>20</li>
                                                <li>21</li>
                                                <li>22</li>
                                                <li>23</li>
                                            </ul>
                                        </div>
                                    </a>

                                </div>

                                <div class="product_grid__item">

                                    <a href="#" class="item">
                                        <div class="item__article">99440-AA1</div>
                                        <div class="item__image">
                                            <div class="item__image_base">
                                                <img src="images/bot_01.jpg" class="img-fluid" alt="">
                                            </div>
                                            <div class="item__image_hover">
                                                <img src="images/bot_02.jpg" class="img-fluid" alt="">
                                            </div>
                                        </div>
                                        <div class="item__name">
                                            <strong>Chaussons bottillons velcro fille licorne</strong>
                                            <span>Velours</span>
                                        </div>
                                        <div class="item__price">
                                            <span>14,99 €</span>
                                        </div>
                                        <div class="item__options">
                                            <div class="item__options_title">Tailles disponibles</div>
                                            <ul>
                                                <li>20</li>
                                                <li>21</li>
                                                <li>22</li>
                                                <li>23</li>
                                            </ul>
                                        </div>
                                    </a>

                                </div>
                                <div class="product_grid__item">

                                    <a href="#" class="item">
                                        <div class="item__article">99440-AA1</div>
                                        <div class="item__image">
                                            <div class="item__image_base">
                                                <img src="images/bot_01.jpg" class="img-fluid" alt="">
                                            </div>
                                            <div class="item__image_hover">
                                                <img src="images/bot_02.jpg" class="img-fluid" alt="">
                                            </div>
                                        </div>
                                        <div class="item__name">
                                            <strong>Chaussons bottillons velcro fille licorne</strong>
                                            <span>Velours</span>
                                        </div>
                                        <div class="item__price">
                                            <span>14,99 €</span>
                                        </div>
                                        <div class="item__options">
                                            <div class="item__options_title">Tailles disponibles</div>
                                            <ul>
                                                <li>20</li>
                                                <li>21</li>
                                                <li>22</li>
                                                <li>23</li>
                                            </ul>
                                        </div>
                                    </a>

                                </div>
                                <div class="product_grid__item">

                                    <a href="#" class="item">
                                        <div class="item__article">99440-AA1</div>
                                        <div class="item__image">
                                            <div class="item__image_base">
                                                <img src="images/bot_01.jpg" class="img-fluid" alt="">
                                            </div>
                                            <div class="item__image_hover">
                                                <img src="images/bot_02.jpg" class="img-fluid" alt="">
                                            </div>
                                        </div>
                                        <div class="item__name">
                                            <strong>Chaussons bottillons velcro fille licorne</strong>
                                            <span>Velours</span>
                                        </div>
                                        <div class="item__price">
                                            <span>14,99 €</span>
                                        </div>
                                        <div class="item__options">
                                            <div class="item__options_title">Tailles disponibles</div>
                                            <ul>
                                                <li>20</li>
                                                <li>21</li>
                                                <li>22</li>
                                                <li>23</li>
                                            </ul>
                                        </div>
                                    </a>

                                </div>

                                <div class="product_grid__item">

                                    <a href="#" class="item">
                                        <div class="item__article">99440-AA1</div>
                                        <div class="item__image">
                                            <div class="item__image_base">
                                                <img src="images/bot_01.jpg" class="img-fluid" alt="">
                                            </div>
                                            <div class="item__image_hover">
                                                <img src="images/bot_02.jpg" class="img-fluid" alt="">
                                            </div>
                                        </div>
                                        <div class="item__name">
                                            <strong>Chaussons bottillons velcro fille licorne</strong>
                                            <span>Velours</span>
                                        </div>
                                        <div class="item__price">
                                            <span>14,99 €</span>
                                        </div>
                                        <div class="item__options">
                                            <div class="item__options_title">Tailles disponibles</div>
                                            <ul>
                                                <li>20</li>
                                                <li>21</li>
                                                <li>22</li>
                                                <li>23</li>
                                            </ul>
                                        </div>
                                    </a>

                                </div>
                                <div class="product_grid__item">

                                    <a href="#" class="item">
                                        <div class="item__article">99440-AA1</div>
                                        <div class="item__image">
                                            <div class="item__image_base">
                                                <img src="images/bot_01.jpg" class="img-fluid" alt="">
                                            </div>
                                            <div class="item__image_hover">
                                                <img src="images/bot_02.jpg" class="img-fluid" alt="">
                                            </div>
                                        </div>
                                        <div class="item__name">
                                            <strong>Chaussons bottillons velcro fille licorne</strong>
                                            <span>Velours</span>
                                        </div>
                                        <div class="item__price">
                                            <span>14,99 €</span>
                                        </div>
                                        <div class="item__options">
                                            <div class="item__options_title">Tailles disponibles</div>
                                            <ul>
                                                <li>20</li>
                                                <li>21</li>
                                                <li>22</li>
                                                <li>23</li>
                                            </ul>
                                        </div>
                                    </a>

                                </div>
                                <div class="product_grid__item">

                                    <a href="#" class="item">
                                        <div class="item__article">99440-AA1</div>
                                        <div class="item__image">
                                            <div class="item__image_base">
                                                <img src="images/bot_01.jpg" class="img-fluid" alt="">
                                            </div>
                                            <div class="item__image_hover">
                                                <img src="images/bot_02.jpg" class="img-fluid" alt="">
                                            </div>
                                        </div>
                                        <div class="item__name">
                                            <strong>Chaussons bottillons velcro fille licorne</strong>
                                            <span>Velours</span>
                                        </div>
                                        <div class="item__price">
                                            <span>14,99 €</span>
                                        </div>
                                        <div class="item__options">
                                            <div class="item__options_title">Tailles disponibles</div>
                                            <ul>
                                                <li>20</li>
                                                <li>21</li>
                                                <li>22</li>
                                                <li>23</li>
                                            </ul>
                                        </div>
                                    </a>

                                </div>

                                <div class="product_grid__item">

                                    <a href="#" class="item">
                                        <div class="item__article">99440-AA1</div>
                                        <div class="item__image">
                                            <div class="item__image_base">
                                                <img src="images/bot_01.jpg" class="img-fluid" alt="">
                                            </div>
                                            <div class="item__image_hover">
                                                <img src="images/bot_02.jpg" class="img-fluid" alt="">
                                            </div>
                                        </div>
                                        <div class="item__name">
                                            <strong>Chaussons bottillons velcro fille licorne</strong>
                                            <span>Velours</span>
                                        </div>
                                        <div class="item__price">
                                            <span>14,99 €</span>
                                        </div>
                                        <div class="item__options">
                                            <div class="item__options_title">Tailles disponibles</div>
                                            <ul>
                                                <li>20</li>
                                                <li>21</li>
                                                <li>22</li>
                                                <li>23</li>
                                            </ul>
                                        </div>
                                    </a>

                                </div>
                                <div class="product_grid__item">

                                    <a href="#" class="item">
                                        <div class="item__article">99440-AA1</div>
                                        <div class="item__image">
                                            <div class="item__image_base">
                                                <img src="images/bot_01.jpg" class="img-fluid" alt="">
                                            </div>
                                            <div class="item__image_hover">
                                                <img src="images/bot_02.jpg" class="img-fluid" alt="">
                                            </div>
                                        </div>
                                        <div class="item__name">
                                            <strong>Chaussons bottillons velcro fille licorne</strong>
                                            <span>Velours</span>
                                        </div>
                                        <div class="item__price">
                                            <span>14,99 €</span>
                                        </div>
                                        <div class="item__options">
                                            <div class="item__options_title">Tailles disponibles</div>
                                            <ul>
                                                <li>20</li>
                                                <li>21</li>
                                                <li>22</li>
                                                <li>23</li>
                                            </ul>
                                        </div>
                                    </a>

                                </div>
                                <div class="product_grid__item">

                                    <a href="#" class="item">
                                        <div class="item__article">99440-AA1</div>
                                        <div class="item__image">
                                            <div class="item__image_base">
                                                <img src="images/bot_01.jpg" class="img-fluid" alt="">
                                            </div>
                                            <div class="item__image_hover">
                                                <img src="images/bot_02.jpg" class="img-fluid" alt="">
                                            </div>
                                        </div>
                                        <div class="item__name">
                                            <strong>Chaussons bottillons velcro fille licorne</strong>
                                            <span>Velours</span>
                                        </div>
                                        <div class="item__price">
                                            <span>14,99 €</span>
                                        </div>
                                        <div class="item__options">
                                            <div class="item__options_title">Tailles disponibles</div>
                                            <ul>
                                                <li>20</li>
                                                <li>21</li>
                                                <li>22</li>
                                                <li>23</li>
                                            </ul>
                                        </div>
                                    </a>

                                </div>

                            </div>

                            <div class="gray_box">
                                <p>Botillons, mocassins, ballerines ou mules, les chaussons pantoufles Isotoner offre un large choix de modèles de chaussons de bébé et de chaussons enfants. Pour les bébés, les bottillons à scratch ou à zip plaisent aux enfants et aux parents. En effet, les chaussons à scratchs ou à zip ont la particularité d’avoir des fermetures très pratiques et sont faciles à enfiler pour les enfants, de plus, ils ont de jolis motifs brodés sur le dessus. Ces chaussons à motifs sont décorés d’un cœur, d’un lapin, d’un pompier ou imprimés, comme nos chaussettes antidérapantes</p>
                            </div>

                        </div>
                    </div>

                </div>
            </section>


            <!-- Footer -->
            <?php include('inc/footer.inc.php') ?>
            <!-- -->

        </div>

        <!-- Scripts -->
        <?php include('inc/scripts.inc.php') ?>
        <!-- -->

    </body>
</html>
