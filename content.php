<!doctype html>
<html class="no-js" lang="">

    <!-- Head -->
    <?php include('inc/head.inc.php') ?>
    <!-- -->

    <body>

        <div class="page">

            <!-- Header -->
            <?php include('inc/header.inc.php') ?>
            <!-- -->

            <!-- Navigation -->
            <?php include('inc/nav.inc.php') ?>
            <!-- -->


            <section class="main">
                <div class="container">
                    <ul class="breadcrumb">
                        <li><a href="#">Главная</a></li>
                        <li><span>Текст</span></li>
                    </ul>

                    <h1>QUESTIONS / RÉPONSES</h1>

                    <div class="main_row">
                        <div class="main_sidebar">
                            <div class="sidebar_nav">
                                <div class="sidebar_nav__title">VOTRE QUESTION SUR</div>
                                <ul>
                                    <li><a href="#">Ma commande</a></li>
                                    <li><a href="#">Ma livraisonv
                                    <li><a href="#">Mon compte Isotoner</a></li>
                                    <li><a href="#">Mes paiements</a></li>
                                    <li><a href="#">Echanges et remboursements</a></li>
                                    <li><a href="#">Mes points fidélité</a></li>
                                    <li><a href="#">Respect de mes données</a></li>
                                    <li><a href="#">Garantie produits défectueux</a></li>
                                    <li><a href="#">Abonnements aux newsletters</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="main_content">
                            <div class="content">
                                <h2 class="color_blue text-uppercase">Ma commande</h2>
                                <h3>Je n'arrive pas à ajouter un produit à mon panier, comment faire ?</h3>
                                <p>N'oubliez pas de sélectionner une taille et une couleur (si plusieurs coloris sont disponibles) avant d'essayer d'ajouter le produit à votre panier.</p>
                                <p>Si le produit que vous désirez commander est en rupture de stock momentanée, vous ne pourrez pas l'ajouter à votre panier. Dans ce cas cliquez sur le bouton "me prévenir quand le produit est en stock" et renseignez votre adresse mail dans le champ prévu. Un email vous sera envoyé pour vous avertir de la mise en stock de ce produit. </p>

                                <h3>Ma commande a-t-elle bien été prise en compte ?</h3>
                                <p>Après avoir validé votre commande en ligne, vous recevrez 2 emails :</p>
                                <p>Un email de confirmation de paiement envoyé par la banque Palatine</p>
                                <p>Un email de confirmation de commande avec le numéro de votre commande.</p>
                                <p>Parallèlement votre commande s’affichera dans « Mon compte » rubrique « mes commandes » avec la date et le numéro de la commande.</p>

                                <h3>Comment utiliser un code promotionnel ?</h3>
                                <p>Si vous avez un code promotionnel Isotoner, vous pourrez le rentrer dans la zone « J'ai un code avantage" dans « Mon Panier » avant de valider votre commande. Une fois le code saisi la promotion sera directement appliquée au montant de votre panier.</p>

                                <h3>Un code promotionnel ne fonctionne pas ?</h3>
                                <p>Pensez à bien vérifier la date de validité de votre code promotionnel et que toutes les conditions sont remplies (montant minimum de commande par exemple). Les codes promotionnels ne sont pas cumulables entre eux.</p>

                                <h3>Au bout de combien de temps ma commande est-elle expédiée ?</h3>
                                <p>Les commandes sont traitées du lundi au vendredi hors jours fériés. Pour toute commande passée avant minuit, celle-ci sera traitée sous 2 jours ouvrés à dater du 1er jour ouvré suivant cette commande. La commande sera ensuite remise au service Colissimo de la poste. Les délais d’acheminement indicatifs de la poste sont de :</p>
                                <p>A partir du 1er jour ouvré suivant la date de votre commande, il faut donc compter environ 4 jours ouvrés en France métropolitaine et 6 jours ouvrés pour les autres pays pour recevoir votre colis. Ces délais sont donnés à titre indicatif et sont sujets à variation notamment en période de forte demande.</p>

                            </div>
                        </div>
                    </div>

                </div>
            </section>


            <!-- Footer -->
            <?php include('inc/footer.inc.php') ?>
            <!-- -->

        </div>

        <!-- Scripts -->
        <?php include('inc/scripts.inc.php') ?>
        <!-- -->

    </body>
</html>
