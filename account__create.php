<!doctype html>
<html class="no-js" lang="">

    <!-- Head -->
    <?php include('inc/head.inc.php') ?>
    <!-- -->

    <body>

        <div class="page">

            <!-- Header -->
            <?php include('inc/header.inc.php') ?>
            <!-- -->

            <!-- Navigation -->
            <?php include('inc/nav.inc.php') ?>
            <!-- -->


            <section class="main">
                <div class="container">

                    <h1>Регистрация</h1>

                    <div class="reg_block">

                        <div class="row">
                            <div class="col col-xs-12 col-sm-6 col-gutter-lr">
                                <div class="form_group">
                                    <label class="form_label">Имя <sup>*</sup></label>
                                    <input type="text" name="" class="form_control" placeholder="">
                                </div>
                            </div>
                            <div class="col col-xs-12 col-sm-6 col-gutter-lr">
                                <div class="form_group">
                                    <label class="form_label">Фамилия</label>
                                    <input type="text" name="" class="form_control" placeholder="">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col col-xs-12 col-sm-6 col-gutter-lr">
                                <div class="form_group">
                                    <label class="form_label">Пароль <sup>*</sup></label>
                                    <input type="text" name="" class="form_control" placeholder="">
                                </div>
                            </div>
                            <div class="col col-xs-12 col-sm-6 col-gutter-lr">
                                <div class="form_group">
                                    <label class="form_label">Повторите пароль <sup>*</sup></label>
                                    <input type="text" name="" class="form_control" placeholder="">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col col-xs-12 col-sm-6 col-gutter-lr">
                                <div class="form_group">
                                    <label class="form_label">Email <sup>*</sup></label>
                                    <input type="text" name="" class="form_control" placeholder="">
                                </div>
                            </div>
                            <div class="col col-xs-12 col-sm-6 col-gutter-lr">
                                <div class="form_group">
                                    <label class="form_label">Повторите Email <sup>*</sup></label>
                                    <input type="text" name="" class="form_control" placeholder="">
                                </div>
                            </div>
                        </div>
                        <div class="form_group">
                            <label class="form_checkbox">
                                <input type="checkbox" name="check">
                                <span>En cochant cette case j'accepte et je reconnais avoir pris connaissance des CGV et j'accepte de recevoir les informations sur nos collections ainsi que nos offres exclusives.Vos coordonnées ne seront jamais divulguées à des tiers*</span>
                            </label>
                        </div>
                        <div class="text-right">
                            <button type="submit" class="btn btn_right">Зарегистрироваться</button>
                        </div>
                    </div>


                </div>
            </section>


            <!-- Footer -->
            <?php include('inc/footer.inc.php') ?>
            <!-- -->

        </div>

        <!-- Scripts -->
        <?php include('inc/scripts.inc.php') ?>
        <!-- -->

    </body>
</html>
