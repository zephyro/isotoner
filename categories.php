<!doctype html>
<html class="no-js" lang="">

    <!-- Head -->
    <?php include('inc/head.inc.php') ?>
    <!-- -->

    <body>

        <div class="page">

            <!-- Header -->
            <?php include('inc/header.inc.php') ?>
            <!-- -->

            <!-- Navigation -->
            <?php include('inc/nav.inc.php') ?>
            <!-- -->


            <section class="main">
                <div class="container">
                    <ul class="breadcrumb">
                        <li><a href="#">Главная</a></li>
                        <li><span>Детям</span></li>
                    </ul>

                    <div class="categories_content">
                        <h1>ENFANT</h1>
                        <p>Profitez de la livraison offerte dès 39€ d'achats sur toutes les gammes enfants, Isotoner propose des chaussons, des chaussettes anti-dérapantes et des accessoires. Toutes les collections enfant sont développées selon les dernières tendances de la mode enfantine et font la part belle aux dernières innovations Isotoner. Semelles amortissantes sur les bottillons enfant, semelles ergonomiques sur les chaussons mules ados, chaussettes enfant anti-dérapantes, lunettes de soleil enfant avec filtre de haute protection catégorie 4 : confort, technicité et sécurité sont toujours au rendez-vous !</p>
                    </div>

                    <ul class="categories">

                        <li>
                            <a href="#">
                                <div class="categories__image">
                                    <img src="images/cat/cat_01.jpg" class="img-fluid" alt="">
                                </div>
                                <div class="categories__text">
                                    <span>Обувь для дома</span>
                                </div>
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <div class="categories__image">
                                    <img src="images/cat/cat_02.jpg" class="img-fluid" alt="">
                                </div>
                                <div class="categories__text">
                                    <span>Перчатки</span>
                                </div>
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <div class="categories__image">
                                    <img src="images/cat/cat_03.jpg" class="img-fluid" alt="">
                                </div>
                                <div class="categories__text">
                                    <span>Зонты</span>
                                </div>
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <div class="categories__image">
                                    <img src="images/cat/cat_04.jpg" class="img-fluid" alt="">
                                </div>
                                <div class="categories__text">
                                    <span>Шапки и шарфы</span>
                                </div>
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <div class="categories__image">
                                    <img src="images/cat/cat_05.jpg" class="img-fluid" alt="">
                                </div>
                                <div class="categories__text">
                                    <span>Обувь</span>
                                </div>
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <div class="categories__image">
                                    <img src="images/cat/cat_06.jpg" class="img-fluid" alt="">
                                </div>
                                <div class="categories__text">
                                    <span>Носки</span>
                                </div>
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <div class="categories__image">
                                    <img src="images/cat/cat_07.jpg" class="img-fluid" alt="">
                                </div>
                                <div class="categories__text">
                                    <span>Солцезащитные очки</span>
                                </div>
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <div class="categories__image">
                                    <img src="images/cat/cat_08.jpg" class="img-fluid" alt="">
                                </div>
                                <div class="categories__text">
                                    <span>Новое</span>
                                </div>
                            </a>
                        </li>

                    </ul>

                </div>
            </section>


            <!-- Footer -->
            <?php include('inc/footer.inc.php') ?>
            <!-- -->

        </div>

        <!-- Scripts -->
        <?php include('inc/scripts.inc.php') ?>
        <!-- -->

    </body>
</html>
