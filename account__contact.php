<!doctype html>
<html class="no-js" lang="">

    <!-- Head -->
    <?php include('inc/head.inc.php') ?>
    <!-- -->

    <body>

        <div class="page">

            <!-- Header -->
            <?php include('inc/header.inc.php') ?>
            <!-- -->

            <!-- Navigation -->
            <?php include('inc/nav.inc.php') ?>
            <!-- -->


            <section class="main">
                <div class="container">

                    <h1>Мой аккаунт</h1>

                    <div class="main_row">
                        <div class="main_sidebar">

                            <div class="account_navigation">
                                <div class="account_navigation__title"><i class="fa fa fa-user-o"></i><span>Mon compte</span></div>
                                <ul>
                                    <li><a href="#">Mes données personnelles</a></li>
                                    <li><a href="#">Mes adresses</a></li>
                                    <li><a href="#">Mes newsletters</a></li>
                                    <li class="active"><a href="#">Contact</a></li>
                                    <li><a href="#">Mes messages</a></li>
                                </ul>
                            </div>

                            <ul class="account_links">
                                <li class="item1"><a href="#">Mes points fidélité</a></li>
                                <li class="item2"><a href="#">Mes favoris</a></li>
                                <li class="item3"><a href="#">Mes commandes</a></li>
                                <li class="item4"><a href="#">Déconnexion</a></li>
                            </ul>

                        </div>
                        <div class="main_content">

                            <div class="account_title"><span>Nous contacter</span></div>
                            <h3>Les 3 questions les plus fréquentes</h3>

                            <div class="accordion">

                                <div class="accordion__item">
                                    <div class="accordion__item_heading open">
                                        <b>1</b>
                                        <span>Comment savoir où en est mon colis ? </span>
                                        <i class="fa fa-angle-down"></i>
                                    </div>
                                    <div class="accordion__item_content" style="display: block">
                                        <p>Un email m'est envoyé dès que ma commande est expédiée avec un lien vers le site de la poste (www.coliposte.net) me permettant de suivre l’acheminement de mon colis. A noter qu’à réception de l’email, il faut compter 24h pour que la poste mette en ligne mon numéro de suivi.</p>
                                        <p>Je peux également suivre l’acheminement de mon colis à partir du lien qui se trouve dans « Mon compte » rubrique « mes commandes ».</p>
                                    </div>
                                </div>

                                <div class="accordion__item">
                                    <div class="accordion__item_heading">
                                        <b>2</b>
                                        <span>Comment faire mon retour ? </span>
                                        <i class="fa fa-angle-down"></i>
                                    </div>
                                    <div class="accordion__item_content">
                                        <p>Je retourne le ou les articles neufs et dans leur emballage d’origine, accompagnés du bon de retour ci-joint à l’adresse suivante :</p>
                                        <p>ISOTONER - Service retour web - 15140 Saint Martin Valmeroux</p>
                                    </div>
                                </div>

                                <div class="accordion__item">
                                    <div class="accordion__item_heading">
                                        <b>3</b>
                                        <span>Comment échanger un produit défectueux (produit acheté en ligne ou en magasin)</span>
                                        <i class="fa fa-angle-down"></i>
                                    </div>
                                    <div class="accordion__item_content">
                                        <p>Parce que la satisfaction de ses clients est essentielle, Isotoner garantit ses produits contre tout vice de fabrication. Isotoner s’engage à échanger tout produit défectueux acheté en ligne ou en magasin dans l’année suivant l’achat du produit. Si vous désirez faire une demande de retour en vue d’un échange, merci de remplir le formulaire ci-dessous et de choisir le thème "Produit défectueux".</p>
                                        <p>
                                            <strong>N'oubliez pas :</strong><br/>
                                            <strong>- de préciser dans votre message si votre achat a été effectué en magasin ou sur Internet</strong>
                                            <strong>- de joindre des photos du produit défectueux afin que nous puissions traiter votre demande</strong>
                                        </p>
                                        <p>Votre demande sera analysée par notre Service Satisfaction Clients qui vous répondra par mail ou par téléphone dans les plus brefs délais. </p>
                                    </div>
                                </div>

                            </div>

                        </div>
                    </div>


                </div>
            </section>


            <!-- Footer -->
            <?php include('inc/footer.inc.php') ?>
            <!-- -->

        </div>

        <!-- Scripts -->
        <?php include('inc/scripts.inc.php') ?>
        <!-- -->

    </body>
</html>
