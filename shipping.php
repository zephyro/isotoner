<!doctype html>
<html class="no-js" lang="">

    <!-- Head -->
    <?php include('inc/head.inc.php') ?>
    <!-- -->

    <body>

        <div class="page">

            <!-- Header -->
            <?php include('inc/header.inc.php') ?>
            <!-- -->

            <!-- Navigation -->
            <?php include('inc/nav.inc.php') ?>
            <!-- -->


            <section class="main">
                <div class="container">

                    <ul class="purchase_step">
                        <li>
                            <a class="purchase_step__item" href="">
                                <i>1</i>
                                <span>Identification</span>
                            </a>
                        </li>
                        <li>
                            <div class="purchase_step__item">
                                <i>2</i>
                                <span>Livraison</span>
                            </div>
                        </li>
                        <li>
                            <div class="purchase_step__item next">
                                <i>3</i>
                                <span>Paiement</span>
                            </div>
                        </li>
                    </ul>

                    <h1>Choisissez votre mode de livraison</h1>

                    <div class="row mb_20">
                        <div class="col col-xs-12 col-md-7 col-lg-7 col-xl-7">
                            <label class="shipping">
                                <input type="radio" name="shipping" value="1">
                                <div class="shipping__content">
                                    <div class="shipping__icon">
                                        <img src="img/shipping__icon_01.png" class="img-fluid" alt="">
                                    </div>
                                    <div class="shipping__place">mon domicile <strong>0,00 €</strong></div>
                                    <div class="shipping__date">Livraison estimée entre le 22 janvier et le 24 janvier</div>
                                    <div class="shipping__text">
                                        <strong>Livraison à l'adresse de votre choix avec remise en boîte aux lettres</strong> ou remise en mains propres selon les situations.<br/>
                                        En cas d'impossibilité, votre colis sera mis à votre disposition en bureau de poste
                                    </div>
                                </div>
                            </label>
                            <label class="shipping">
                                <input type="radio" name="shipping" value="1">
                                <div class="shipping__content">
                                    <div class="shipping__icon">
                                        <img src="img/shipping__icon_02.png" class="img-fluid" alt="">
                                    </div>
                                    <div class="shipping__place">mon domicile <strong>0,00 €</strong></div>
                                    <div class="shipping__date">Livraison estimée entre le 22 janvier et le 24 janvier</div>
                                    <div class="shipping__text">Livraison dans l'un des 3 700 commerces du réseau PICKUP, Groupe La Poste.</div>
                                </div>
                            </label>
                            <label class="shipping">
                                <input type="radio" name="shipping" value="1">
                                <div class="shipping__content">
                                    <div class="shipping__icon">
                                        <img src="img/shipping__icon_03.png" class="img-fluid" alt="">
                                    </div>
                                    <div class="shipping__place">mon domicile <strong>0,00 €</strong></div>
                                    <div class="shipping__date">Livraison estimée entre le 22 janvier et le 24 janvier</div>
                                    <div class="shipping__text">Livraison dans l'un des 10 000 points de retrait La Poste de votre choix en France.</div>
                                </div>
                            </label>
                        </div>
                        <div class="col col-xs-12 col-md-5 col-lg-5 col-xl-4 col-xl-offset-1">
                            <div class="cart_block">
                                <div class="text-center">
                                    <div class="cart_block__title">Mon panier</div>
                                </div>
                                <div class="cart_block__content">
                                    <div class="cart_block__image">
                                        <a href="#">
                                            <img src="images/cart_image_02.jpg" class="img-fluid" alt="">
                                        </a>
                                    </div>
                                    <div class="cart_block__info">
                                        <a href="#" class="cart_block__info_name">Gants cuir homme</a>
                                        <ul class="cart_block__info_data">
                                            <li>Couleur :Noir</li>
                                            <li>Taille :TU</li>
                                            <li>Quantité :7</li>
                                        </ul>
                                    </div>
                                    <div class="cart_block__price">36,90 €</div>
                                </div>
                                <div class="cart_block__content">
                                    <div class="cart_block__image">
                                        <a href="#">
                                            <img src="images/cart_image_02.jpg" class="img-fluid" alt="">
                                        </a>
                                    </div>
                                    <div class="cart_block__info">
                                        <a href="#" class="cart_block__info_name">Gants cuir homme</a>
                                        <ul class="cart_block__info_data">
                                            <li>Couleur :Noir</li>
                                            <li>Taille :TU</li>
                                            <li>Quantité :7</li>
                                        </ul>
                                    </div>
                                    <div class="cart_block__price">36,90 €</div>
                                </div>
                                <div class="cart_block__content">
                                    <div class="cart_block__image">
                                        <a href="#">
                                            <img src="images/cart_image_03.jpg" class="img-fluid" alt="">
                                        </a>
                                    </div>
                                    <div class="cart_block__info">
                                        <a href="#" class="cart_block__info_name">Gants cuir homme</a>
                                        <ul class="cart_block__info_data">
                                            <li>Couleur :Noir</li>
                                            <li>Taille :TU</li>
                                            <li>Quantité :7</li>
                                        </ul>
                                    </div>
                                    <div class="cart_block__price">36,90 €</div>
                                </div>

                                <table class="cart_block__summary">
                                    <tr>
                                        <td>Sous-total</td>
                                        <td>740,70 €</td>
                                    </tr>
                                    <tr>
                                        <td>Frais de livraison estimés (mon domicile)</td>
                                        <td>0,00 €</td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <strong>Montant total</strong>
                                        </td>
                                        <td class="color_red"><strong>740,70 €</strong></td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </div>

                    <div class="text-right">
                        <button type="submit" class="btn btn_right">Оформить заказ</button>
                    </div>


                </div>
            </section>


            <!-- Footer -->
            <?php include('inc/footer.inc.php') ?>
            <!-- -->

        </div>

        <!-- Scripts -->
        <?php include('inc/scripts.inc.php') ?>
        <!-- -->

    </body>
</html>
