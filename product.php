<!doctype html>
<html class="no-js" lang="">

    <!-- Head -->
    <?php include('inc/head.inc.php') ?>
    <!-- -->

    <body>

        <div class="page">

            <!-- Header -->
            <?php include('inc/header.inc.php') ?>
            <!-- -->

            <!-- Navigation -->
            <?php include('inc/nav.inc.php') ?>
            <!-- -->


            <section class="main">
                <div class="container">

                    <ul class="breadcrumb">
                        <li><a href="#">ACCUEIL</a></li>
                        <li><a href="#">FEMME</a></li>
                        <li><a href="#">CHAUSSONS</a></li>
                        <li><span>CHAUSSONS BALLERINES FEMME</span></li>
                    </ul>

                    <div class="product">
                        <div class="product__gallery">
                            <div class="product__gallery_content">
                                <div class="product__gallery_item active" id="image1">
                                    <img class = "img-fluid cloudzoom" src="images/product/product__01.jpg" data-cloudzoom="zoomImage:'images/product/product__01_xl.jpg',zoomPosition:'inside'" alt=""/>
                                </div>
                                <div class="product__gallery_item" id="image2">
                                    <img class = "img-fluid cloudzoom" src="images/product/product__02.jpg" data-cloudzoom="zoomImage:'images/product/product__02_xl.jpg',zoomPosition:'inside'" alt=""/>
                                </div>
                                <div class="product__gallery_item" id="image3">
                                    <img class = "img-fluid cloudzoom" src="images/product/product__03.jpg" data-cloudzoom="zoomImage:'images/product/product__03_xl.jpg',zoomPosition:'inside'" alt=""/>
                                </div>
                                <div class="product__gallery_item" id="image4">
                                    <img class = "img-fluid cloudzoom" src="images/product/product__04.jpg" data-cloudzoom="zoomImage:'images/product/product__04_xl.jpg',zoomPosition:'inside'" alt=""/>
                                </div>
                                <div class="product__gallery_item" id="image5">
                                    <img class = "img-fluid cloudzoom" src="images/product/product__05.jpg" data-cloudzoom="zoomImage:'images/product/product__05_xl.jpg',zoomPosition:'inside'" alt=""/>
                                </div>
                            </div>
                            <div class="product__gallery_thumbs">
                                <div class="gallery_thumbs swiper-container">
                                    <div class="swiper-wrapper">
                                        <div class="swiper-slide">
                                            <a href="#image1" class="product__gallery_switch active">
                                                <img src="images/product/product__01.jpg" class="img-fluid" alt="">
                                            </a>
                                        </div>
                                        <div class="swiper-slide">
                                            <a href="#image2" class="product__gallery_switch">
                                                <img src="images/product/product__02.jpg" class="img-fluid" alt="">
                                            </a>
                                        </div>
                                        <div class="swiper-slide">
                                            <a href="#image3" class="product__gallery_switch">
                                                <img src="images/product/product__03.jpg" class="img-fluid" alt="">
                                            </a>
                                        </div>
                                        <div class="swiper-slide">
                                            <a href="#image4" class="product__gallery_switch">
                                                <img src="images/product/product__04.jpg" class="img-fluid" alt="">
                                            </a>
                                        </div>
                                        <div class="swiper-slide">
                                            <a href="#image5" class="product__gallery_switch">
                                                <img src="images/product/product__05.jpg" class="img-fluid" alt="">
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                        <div class="product__content">
                            <h1 class="product__name">Chaussons ballerines femme</h1>
                            <div class="product__subtitle">Velours - grand noeud</div>
                            <div class="product__article">95811-RSF</div>
                            <div class="product__price">27,90 €</div>
                            <div class="product__options">
                                <div class="product__options_title"><strong>COULEUR:</strong> <span>ROSEFONCÉ</span></div>
                                <ul class="product__options_colors">
                                    <li class="active">
                                        <a href="#">
                                            <img src="images/product/color_01.jpg" class="img-fluid" alt="">
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#">
                                            <img src="images/product/color_02.jpg" class="img-fluid" alt="">
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#">
                                            <img src="images/product/color_03.jpg" class="img-fluid" alt="">
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#">
                                            <img src="images/product/color_04.jpg" class="img-fluid" alt="">
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#">
                                            <img src="images/product/color_05.jpg" class="img-fluid" alt="">
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#">
                                            <img src="images/product/color_06.jpg" class="img-fluid" alt="">
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#">
                                            <img src="images/product/color_07.jpg" class="img-fluid" alt="">
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#">
                                            <img src="images/product/color_08.jpg" class="img-fluid" alt="">
                                        </a>
                                    </li>
                                </ul>
                            </div>

                            <div class="product__options">
                                <div class="product__options_title"><strong>Размеры</strong><a href="#">Таблица размеров</a></div>
                                <ul class="product__options_size">
                                    <li>
                                        <label>
                                            <input type="radio" name="pr_size" value="">
                                            <span>35/36</span>
                                        </label>
                                    </li>
                                    <li>
                                        <label>
                                            <input type="radio" name="pr_size" value="">
                                            <span>37/38</span>
                                        </label>
                                    </li>
                                    <li>
                                        <label>
                                            <input type="radio" name="pr_size" value="">
                                            <span>39/40</span>
                                        </label>
                                    </li>
                                    <li>
                                        <label>
                                            <input type="radio" name="pr_size" value="">
                                            <span>41/42</span>
                                        </label>
                                    </li>
                                </ul>
                            </div>
                            <div class="product__buy">
                                <a href="#" class="btn btn_purchase"><span>В корзину</span></a>
                            </div>
                            <div class="product__actions">
                                <a href="#" class="product__like"><i class="fa fa-heart"></i> <span>В избранное</span></a>
                                <div class="product__social">
                                    <a href="#"><i class="fa fa-instagram"></i></a>
                                    <a href="#"><i class="fa fa-facebook"></i></a>
                                    <a href="#"><i class="fa fa-twitter"></i></a>
                                    <a href="#"><i class="fa fa-google-plus"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </section>

            <div class="product_tabs">
                <div class="container">
                    <ul class="product_tabs__nav">
                        <li class="active"><a href="#tab1">Описание</a></li>
                        <li><a href="#tab2">Свойства</a></li>
                        <li><a href="#tab3">Доствка и возврат</a></li>
                    </ul>

                    <div class="product_tabs__item active" id="tab1">
                        <div class="product_tabs__wrap">
                            <p>La ballerine classique d'Isotoner. Effet seconde peau grâce à son velours de coton ultra stretch.</p>
                        </div>
                    </div>
                    <div class="product_tabs__item" id="tab2">
                        <ul class="product_tabs__info">
                            <li><strong>Composition:</strong> 78% Coton - 18% Polyester - 4% Spandex</li>
                            <li><strong>Doublure intérieure:</strong> 78% coton - 17% polyester - 5% spandex</li>
                            <li><strong>Semelle intérieure:</strong> 75% Coton - 25% Polyester</li>
                            <li><strong>Semell:</strong> Cuir</li>
                            <li><strong>Talon:</strong> 0 cm</li>
                            <li><strong>Entretien:</strong> Lavable machine 30°C</li>
                        </ul>
                    </div>
                    <div class="product_tabs__item" id="tab3">
                        <div class="product_tabs__logo">
                            <img src="/img/product__vendor_logo.png" class="img-fluid" alt="">
                        </div>
                        <ul class="product_tabs__data">
                            <li>
                                <img src="img/product__icon_01.png" alt="">
                                <strong>Livraison à domicile <span class="color_blue">3,90 €</span></strong>
                            </li>
                            <li>
                                <img src="img/product__icon_02.jpg" alt="">
                                <strong>Livraison dans votre bureau de poste <span class="color_blue">2,90 €</span></strong>
                            </li>
                            <li>
                                <img src="img/product__icon_03.jpg" alt="">
                                <strong>Livraison chez un commerçant <span class="color_blue">2,90 €</span></strong>
                            </li>
                        </ul>
                    </div>

                </div>
            </div>

            <section class="main">
                <div class="container">

                    <div class="homepage_title"><span>Vous aimerez aussi</span></div>

                    <div class="fancy_row">

                        <div class="fancy_item">
                            <a class="fancy" href="#">
                                <div class="fancy__image">
                                    <img src="images/fancy__01.jpg" class="img-fluid" alt="">
                                </div>
                                <div class="fancy__name">Parapluie Mini</div>
                                <div class="fancy__text">Ouverture/fermeture automatique - Technologie X-TRA  SEC- ULTRA DEPERLANT</div>
                                <div class="fancy__price"><span>34,90 €</span></div>
                            </a>
                        </div>

                        <div class="fancy_item">
                            <a class="fancy" href="#">
                                <div class="fancy__image">
                                    <img src="images/fancy__02.jpg" class="img-fluid" alt="">
                                </div>
                                <div class="fancy__name">Gants femme maille diagonal</div>
                                <div class="fancy__text">Tricot - doublés fausse fourrure</div>
                                <div class="fancy__price"><span>29,90 €</span></div>
                            </a>
                        </div>

                    </div>

                </div>
            </section>


            <!-- Footer -->
            <?php include('inc/footer.inc.php') ?>
            <!-- -->

            <div class="hide">
                <a href="#purchase" class="btn_open_purchase btn_modal"></a>

                <div class="purchase" id="purchase">
                    <div class="purchase__wrap">

                        <div class="purchase__heading"><span>Votre article a bien été ajouté au panier</span></div>

                        <div class="purchase__content">

                            <div class="purchase__product">
                                <div class="purchase__product_image">
                                    <img src="images/goods__02.jpg" class="img-fluid" alt="">
                                </div>
                                <div class="purchase__product_detail">
                                    <div class="purchase__product_title">Chaussons mules homme classiques</div>
                                    <div class="purchase__product_subtitle">Velours - semelle ERGONOMIQUE</div>
                                    <div class="purchase__product_color">Couleur : Gris</div>
                                    <div class="purchase__product_size">Taille : 41</div>
                                </div>
                                <div class="purchase__product_price">
                                    <span>23,99 €</span>
                                </div>
                            </div>

                            <div class="purchase__button">
                                <div class="purchase__button_item">
                                    <a href="#" class="btn btn_blue btn_left"><span>Продолжить покупки</span></a>
                                </div>
                                <div class="purchase__button_item">
                                    <a href="#" class="btn btn_right"><span>Оформить заказ</span></a>
                                </div>
                            </div>

                            <div class="purchase__title"><span>ISOTONER VOUS RECOMMANDE EGALEMENT</span></div>

                            <div class="purchase__fancy">

                                <div class="purchase__fancy_item">
                                    <a class="fancy" href="#">
                                        <div class="fancy__image">
                                            <img src="images/fancy__01.jpg" class="img-fluid" alt="">
                                        </div>
                                        <div class="fancy__name">Parapluie Mini</div>
                                        <div class="fancy__text">Ouverture/fermeture automatique - Technologie X-TRA  SEC- ULTRA DEPERLANT</div>
                                        <div class="fancy__price"><span>34,90 €</span></div>
                                    </a>
                                </div>

                                <div class="purchase__fancy_item">
                                    <a class="fancy" href="#">
                                        <div class="fancy__image">
                                            <img src="images/fancy__02.jpg" class="img-fluid" alt="">
                                        </div>
                                        <div class="fancy__name">Gants femme maille diagonal</div>
                                        <div class="fancy__text">Tricot - doublés fausse fourrure</div>
                                        <div class="fancy__price"><span>29,90 €</span></div>
                                    </a>
                                </div>

                                <div class="purchase__fancy_item">
                                    <a class="fancy" href="#">
                                        <div class="fancy__image">
                                            <img src="images/fancy__01.jpg" class="img-fluid" alt="">
                                        </div>
                                        <div class="fancy__name">Parapluie Mini</div>
                                        <div class="fancy__text">Ouverture/fermeture automatique - Technologie X-TRA  SEC- ULTRA DEPERLANT</div>
                                        <div class="fancy__price"><span>34,90 €</span></div>
                                    </a>
                                </div>

                            </div>

                        </div>
                    </div>

                </div>

            </div>

        </div>

        <!-- Scripts -->
        <?php include('inc/scripts.inc.php') ?>
        <!-- -->

    </body>
</html>
