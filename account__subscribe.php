<!doctype html>
<html class="no-js" lang="">

    <!-- Head -->
    <?php include('inc/head.inc.php') ?>
    <!-- -->

    <body>

        <div class="page">

            <!-- Header -->
            <?php include('inc/header.inc.php') ?>
            <!-- -->

            <!-- Navigation -->
            <?php include('inc/nav.inc.php') ?>
            <!-- -->


            <section class="main">
                <div class="container">

                    <h1>Мой аккаунт</h1>

                    <div class="main_row">
                        <div class="main_sidebar">

                            <div class="account_navigation">
                                <div class="account_navigation__title"><i class="fa fa fa-user-o"></i><span>Mon compte</span></div>
                                <ul>
                                    <li><a href="#">Mes données personnelles</a></li>
                                    <li><a href="#">Mes adresses</a></li>
                                    <li><a href="#">Mes newsletters</a></li>
                                    <li class="active"><a href="#">Contact</a></li>
                                    <li><a href="#">Mes messages</a></li>
                                </ul>
                            </div>

                            <ul class="account_links">
                                <li class="item1"><a href="#">Mes points fidélité</a></li>
                                <li class="item2"><a href="#">Mes favoris</a></li>
                                <li class="item3"><a href="#">Mes commandes</a></li>
                                <li class="item4"><a href="#">Déconnexion</a></li>
                            </ul>

                        </div>
                        <div class="main_content">

                            <div class="account_title"><span>Подписка на рассылку</span></div>
                            <form class="form">
                                <div class="form_group">
                                    <label class="form_checkbox">
                                        <input type="checkbox" name="name" value="">
                                        <span>Я подписываюсь на новостную рассылку Isotoner, чтобы: </span>
                                    </label>
                                </div>
                                <div class="form_group mb_40 pl_30">
                                    - получать предварительный просмотр предложений привилегий и частных кодов скидок; - получать
                                    информацию о специальных событиях (распродажах и акциях в середине сезона), а также о последних новостях и нововведениях Isotoner.
                                </div>
                                <div class="text-right">
                                    <button type="submit" class="btn btn_right">Подписаться</button>
                                </div>
                            </form>

                        </div>
                    </div>


                </div>
            </section>


            <!-- Footer -->
            <?php include('inc/footer.inc.php') ?>
            <!-- -->

        </div>

        <!-- Scripts -->
        <?php include('inc/scripts.inc.php') ?>
        <!-- -->

    </body>
</html>
