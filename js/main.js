
$(".btn_modal").fancybox({
    'padding'    : 0
});

var mainSlider = new Swiper('.main_slider__container', {
    slidesPerView: 1,
    pagination: {
        el: '.swiper-pagination',
        clickable: true
    },
});

(function() {

    $('.filter_toggle').on('click touchstart', function(e){
        e.preventDefault();

        $('.page').toggleClass('filter_open');
    });

}());

jQuery(function(){
    CloudZoom.quickStart();
});


(function() {

    $('.tabs-nav > li > a').on('click touchstart', function(e){
        e.preventDefault();

        var tab = $($(this).attr("data-target"));
        var box = $(this).closest('.tabs');

        $(this).closest('.tabs-nav').find('li').removeClass('active');
        $(this).closest('li').addClass('active');

        box.find('> .tabs-item').removeClass('active');
        box.find(tab).addClass('active');
    });

}());

var swiper = new Swiper('.gallery_thumbs', {
    slidesPerView: 5,
    spaceBetween: 4
});

(function() {

    $('.product__gallery_switch').on('click touchstart', function(e){
        e.preventDefault();

        var tab = $($(this).attr("href"));
        var box = $(this).closest('.product__gallery');

        $(this).closest('.product__gallery_thumbs').find('a').removeClass('active');
        $(this).addClass('active');

        box.find('.product__gallery_item').removeClass('active');
        box.find(tab).addClass('active');
    });

}());


(function() {

    $('.product_tabs__nav li a').on('click touchstart', function(e){
        e.preventDefault();

        var tab = $($(this).attr("href"));
        var box = $(this).closest('.product_tabs');

        $(this).closest('.product_tabs__nav').find('li').removeClass('active');
        $(this).closest('li').addClass('active');

        box.find('.product_tabs__item').removeClass('active');
        box.find(tab).addClass('active');
    });

}());


// Tabs

(function() {

    $('.accordion__item_heading').on('click touchstart', function(e){
        e.preventDefault();

        $(this).toggleClass('open');
        $(this).closest('.accordion__item').find('.accordion__item_content').slideToggle('fast');
    });

}());


$('.btn_purchase').on('click touchstart', function(e){
    e.preventDefault();
    console.log('click');
    $('.btn_open_purchase').trigger('click');
});



$('.header_account__toggle').on('click touchstart', function(e){
    e.preventDefault();
    $(this).closest('.header_account').toggleClass('open');
});

// hide dropdown

$('body').click(function (event) {

    if ($(event.target).closest(".header_account").length === 0) {
        $(".header_account").removeClass('open');
    }
});


$('.header_cart__toggle').on('click touchstart', function(e){
    e.preventDefault();
    $(this).closest('.header_cart').toggleClass('open');
});

// hide dropdown

$('body').click(function (event) {

    if ($(event.target).closest(".header_cart").length === 0) {
        $(".header_cart").removeClass('open');
    }
});
