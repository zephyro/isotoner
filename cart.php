<!doctype html>
<html class="no-js" lang="">

    <!-- Head -->
    <?php include('inc/head.inc.php') ?>
    <!-- -->

    <body>

        <div class="page">

            <!-- Header -->
            <?php include('inc/header.inc.php') ?>
            <!-- -->

            <!-- Navigation -->
            <?php include('inc/nav.inc.php') ?>
            <!-- -->


            <section class="main">
                <div class="container">

                    <h1 class="color_blue">Корзина</h1>

                    <table class="cart_table">

                        <tr>
                            <th class="hide_mobile">Articles</th>
                            <th>Description</th>
                            <th class="hide-xs-only">Couleur</th>
                            <th class="hide-xs-only">Taille</th>
                            <th class="hide-xs-only">Quantité</th>
                            <th class="hide-xs-only">Prix unitaire</th>
                            <th>Prix total</th>
                            <th></th>
                        </tr>

                        <tr>
                            <td class="hide-xs-only">
                                <a class="cart_image" href="#">
                                    <img src="images/cart_image_01.jpg" class="img-fluid">
                                </a>
                            </td>
                            <td>
                                <div class="cart_product">Parapluie Crook X-TRA-SOLIDE<a href="#"></a></div>
                                <div class="cart_article">Réf. 09407-NR3</div>
                            </td>
                            <td class="hide-xs-only">
                                <span class="cart_color">red</span>
                            </td>
                            <td class="hide-xs-only">
                                <span class="cart_size">37</span>
                            </td>
                            <td>
                                <select type="text" class="form_control form_select cart_amount" name="">
                                    <option value="1">1</option>
                                    <option value="2">2</option>
                                    <option value="3">3</option>
                                    <option value="4">4</option>
                                    <option value="5">5</option>
                                </select>
                            </td>
                            <td class="hide-xs-only">
                                <span class="cart_price">36,90 €</span>
                            </td>
                            <td>
                                <span class="cart_price">36,90 €</span>
                            </td>
                            <td>
                                <a href="#" class="cart_remove"></a>
                            </td>
                        </tr>

                        <tr>
                            <td class="hide-xs-only">
                                <a class="cart_image" href="#">
                                    <img src="images/cart_image_02.jpg" class="img-fluid">
                                </a>
                            </td>
                            <td>
                                <div class="cart_product">Parapluie Crook X-TRA-SOLIDE<a href="#"></a></div>
                                <div class="cart_article">Réf. 09407-NR3</div>
                            </td>
                            <td class="hide-xs-only">
                                <span class="cart_color">red</span>
                            </td>
                            <td class="hide-xs-only">
                                <span class="cart_size">37</span>
                            </td>
                            <td>
                                <select type="text" class="form_control form_select cart_amount" name="">
                                    <option value="1">1</option>
                                    <option value="2">2</option>
                                    <option value="3">3</option>
                                    <option value="4">4</option>
                                    <option value="5">5</option>
                                </select>
                            </td>
                            <td class="hide-xs-only">
                                <span class="cart_price">36,90 €</span>
                            </td>
                            <td>
                                <span class="cart_price">36,90 €</span>
                            </td>
                            <td>
                                <a href="#" class="cart_remove"></a>
                            </td>
                        </tr>

                        <tr>
                            <td class="hide-xs-only">
                                <a class="cart_image" href="#">
                                    <img src="images/cart_image_03.jpg" class="img-fluid">
                                </a>
                            </td>
                            <td>
                                <div class="cart_product">Parapluie Crook X-TRA-SOLIDE<a href="#"></a></div>
                                <div class="cart_article">Réf. 09407-NR3</div>
                            </td>
                            <td class="hide-xs-only">
                                <span class="cart_color">red</span>
                            </td>
                            <td class="hide-xs-only">
                                <span class="cart_size">37</span>
                            </td>
                            <td>
                                <select type="text" class="form_control form_select cart_amount" name="">
                                    <option value="1">1</option>
                                    <option value="2">2</option>
                                    <option value="3">3</option>
                                    <option value="4">4</option>
                                    <option value="5">5</option>
                                </select>
                            </td>
                            <td class="hide-xs-only">
                                <span class="cart_price">36,90 €</span>
                            </td>
                            <td>
                                <span class="cart_price">36,90 €</span>
                            </td>
                            <td>
                                <a href="#" class="cart_remove"></a>
                            </td>
                        </tr>

                    </table>

                    <div class="row">
                        <div class="col col-xs-12 col-md-4 col-xl-5 col-gutter-lr">
                            <div class="cart_discount">
                                <div class="cart_discount__title">J'ai un code avantage</div>
                                <form class="form">
                                    <div class="cart_discount__form">
                                        <input type="text" class="form_control cart_discount__input" name="" value="">
                                        <button type="submit" class="cart_discount__btn">ОК</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                        <div class="col col-xs-12 col-md-8 col-xl-7 col-gutter-lr">
                            <table class="cart_total">
                                <tr>
                                    <td>SOUS-TOTAL</td>
                                    <td>740,70 €</td>
                                </tr>
                                <tr>
                                    <td>FRAIS DE LIVRAISON ESTIMÉS (MON COMMERÇANT)</td>
                                    <td>0,00 €</td>
                                </tr>
                                <tr>
                                    <td colspan="2"></td>
                                </tr>
                                <tr>
                                    <th>MONTANT TOTAL</th>
                                    <th>740,70 €</th>
                                </tr>
                            </table>
                            <ul class="cart_actions">
                                <li><a href="#" class="btn btn_blue btn_left"><span>Продолжить покупки</span></a></li>
                                <li><a href="#" class="btn btn_right"><span>Оформить заказ</span></a></li>
                            </ul>
                        </div>
                    </div>

                </div>
            </section>


            <!-- Footer -->
            <?php include('inc/footer.inc.php') ?>
            <!-- -->

        </div>

        <!-- Scripts -->
        <?php include('inc/scripts.inc.php') ?>
        <!-- -->

    </body>
</html>
