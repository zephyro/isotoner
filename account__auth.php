<!doctype html>
<html class="no-js" lang="">

    <!-- Head -->
    <?php include('inc/head.inc.php') ?>
    <!-- -->

    <body>

        <div class="page">

            <!-- Header -->
            <?php include('inc/header.inc.php') ?>
            <!-- -->

            <!-- Navigation -->
            <?php include('inc/nav.inc.php') ?>
            <!-- -->


            <section class="main">
                <div class="container">

                    <h1>Мой аккаунт</h1>

                    <div class="auth">

                        <div class="auth__group">
                            <div class="auth__heading"><span>Уже зарегистрированы?</span></div>
                            <div class="auth__subtitle">Войдите в свой аккаунт!</div>
                            <form class="form">
                                <div class="form_group">
                                    <label class="form_label">Email:</label>
                                    <input type="text" name="name" class="form_control" placeholder="">
                                </div>
                                <div class="form_group">
                                    <label class="form_label">Пароль:</label>
                                    <input type="text" name="name" class="form_control" placeholder="">
                                </div>
                                <div class="form_group">
                                    <a href="#" class="form_forgot">Забыли пароль</a>
                                </div>
                                <div class="text-right">
                                    <button type="submit" class="btn btn_right">Войти</button>
                                </div>
                            </form>
                        </div>

                        <div class="auth__group">
                            <div class="auth__heading"><span>Регистрация</span></div>
                            <div class="auth__subtitle">Создайте свой аккаунт!</div>
                            <form class="form">
                                <div class="form_group">
                                    <label class="form_label">Email:</label>
                                    <input type="text" name="name" class="form_control" placeholder="">
                                </div>
                                <div class="form_group mb_30">
                                    <label class="form_checkbox">
                                        <input type="checkbox" name="name" value="">
                                        <span>Я подписываюсь на рассылку, чтобы получать предложения, коды скидок и доступ к частным продажам и специальным событиям.</span>
                                    </label>
                                </div>
                                <div class="text-right">
                                    <button type="submit" class="btn btn_right">Создать аккаунт</button>
                                </div>
                            </form>
                        </div>
                    </div>

                </div>
            </section>


            <!-- Footer -->
            <?php include('inc/footer.inc.php') ?>
            <!-- -->

        </div>

        <!-- Scripts -->
        <?php include('inc/scripts.inc.php') ?>
        <!-- -->

    </body>
</html>
