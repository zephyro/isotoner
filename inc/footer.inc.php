<footer class="footer">
    <div class="container">

        <ul class="footer__top">
            <li>
                <a href="#">
                    <img src="img/footer__icon_01.jpg" alt="">
                    <span>RewardLoyalty rewarded</span>
                    <strong>€5 awarded from €50 of cumulated purchases</strong>
                </a>
            </li>
            <li>
                <a href="#">
                    <img src="img/footer__icon_02.jpg" alt="">
                    <span>Customer serviceCustomer service</span>
                    <strong>Contact us by e-mail</strong>
                </a>
            </li>
            <li>
                <a href="#">
                    <img src="img/footer__icon_03.jpg" alt="">
                    <span>Secure payment</span>
                </a>
            </li>
            <li>
                <a href="#">
                    <img src="img/footer__icon_04.jpg" alt="">
                    <span>Fast delivery</span>
                    <strong>free over €49</strong>
                </a>
            </li>
            <li>
                <a href="#">
                    <img src="img/footer__icon_05.jpg" alt="">
                    <span>15 days to change your mind</span>
                </a>
            </li>
        </ul>

        <div class="footer__content">

            <ul class="footer__nav">
                <li>
                    <div class="footer__heading">MY ORDER</div>
                    <ul>
                        <li><a href="#">Order tracking</a></li>
                        <li><a href="#">Returns</a></li>
                        <li><a href="#">Loyalty points</a></li>
                    </ul>
                </li>
                <li>
                    <div class="footer__heading">SERVICES</div>
                    <ul>
                        <li><a href="#">FAQ</a></li>
                        <li><a href="#">Contact us</a></li>
                        <li><a href="#">Secure payment</a></li>
                        <li><a href="#">15 days exchanges</a></li>
                        <li><a href="#">Fast delivery</a></li>
                    </ul>
                </li>
                <li>
                    <div class="footer__heading">GUIDES AND ADVICES</div>
                    <ul>

                        <li><a href="#">Size guide</a></li>
                        <li><a href="#">Umbrellas guide</a></li>
                        <li><a href="#">Gloves guide</a></li>
                    </ul>
                </li>
                <li>
                    <div class="footer__heading">ISOTONER</div>
                    <ul>
                        <li><a href="#">History</a></li>
                        <li><a href="#">Find a store</a></li>
                    </ul>
                </li>
            </ul>

            <ul class="footer__action">
                <li>
                    <div class="footer__heading">SIGN UP FOR OUR NEWSLETTER:</div>
                    <form class="form">
                        <div class="footer__subscribe">
                            <input type="text" name="search" placeholder="Your Email">
                            <button type="submit"><i class="fa fa-search"></i></button>
                        </div>
                    </form>
                </li>
                <li>
                    <div class="footer__heading">FOLLOW US !</div>
                    <div class="footer__social">
                        <a href="#"><i class="fa fa-instagram"></i></a>
                        <a href="#"><i class="fa fa-facebook"></i></a>
                        <a href="#"><i class="fa fa-twitter"></i></a>
                        <a href="#"><i class="fa fa-google-plus"></i></a>
                    </div>
                </li>
            </ul>

        </div>

        <div class="footer__bottom">
            <div class="footer__bottom_copy">Copyright Isotoner 2018</div>
            <ul class="footer__bottom_links">
                <li><a href="#">Terms and Conditions</a></li>
                <li><a href="#">Legal notice</a></li>
                <li><a href="#">Sitemap</a></li>
            </ul>
        </div>

    </div>
</footer>