<header class="header">

    <div class="header__top">
        <div class="container">
            <span>LIVRAISON GRATUITE DÈS 39€ D’ACHATS</span>
        </div>
    </div>

    <div class="header__content">
        <div class="container">
            <div class="header__row">
                <div class="header__left">

                    <a href="#" class="header__toggle nav_toggle">
                        <span></span>
                        <span></span>
                        <span></span>
                    </a>

                    <div class="header__search">
                        <form class="form">
                            <div class="header__search_row">
                                <button type="submit"><i class="fa fa-search"></i></button>
                                <input type="text" name="" placeholder="Поиск....">
                            </div>
                        </form>
                    </div>

                </div>
                <div class="header__center">
                    <a class="header__logo" href="#">
                        <img src="img/logo.png" class="img-fluid">
                    </a>
                </div>
                <div class="header__right">
                    <ul class="header__access">
                        <li>
                            <div class="header_account">
                                <a class="header_account__link header_account__toggle" href="#"><i class="fa fa-user"></i><span>Аккаунт</span></a>
                                <div class="header_account__dropdown">
                                    <div class="header_account__title">УЖЕ ЕСТЬ АККАУНТ</div>
                                    <form class="form">
                                        <div class="form_group">
                                            <div class="form_label">Логин</div>
                                            <input class="form_control form_control_sm" type="text" name="name" placeholder="Email :">
                                        </div>
                                        <div class="form_group">
                                            <div class="form_label">Пароль:</div>
                                            <input class="form_control form_control_sm" type="text" name="name" placeholder="">
                                        </div>
                                        <div class="form_flex">
                                            <a href="#">Забыли пароль?</a>
                                            <a href="#" class="btn btn_sm btn_right"><span>Войти</span></a>
                                        </div>
                                    </form>
                                    <div class="header_account__divider"></div>
                                    <div class="header_account__title">ВЫ НОВЫЙ КЛИЕНТ</div>
                                    <div class="text-center">
                                        <a class="btn btn_sm btn_right" href="#"><span>Создать аккаунт</span></a>
                                    </div>
                                </div>
                            </div>
                        </li>

                        <li>
                            <div class="header_cart">
                                <a class="header_cart__link header_cart__toggle" href="#"><i class="fa fa-shopping-basket"></i><span>Корзина</span></a>
                                <div class="header_cart__dropdown">
                                    <ul class="mini_cart">
                                        <li>
                                            <span class="mini_cart__remove"></span>
                                            <a class="mini_cart__image" href="#">
                                                <img src="images/cart_image_01.jpg" class="img-fluid" alt="">
                                            </a>
                                            <div class="mini_cart__content">
                                                <a class="mini_cart__name" href="#">Gants cuir homme</a>
                                                <div class="mini_cart__price">89,90 €</div>
                                            </div>
                                        </li>
                                        <li>
                                            <span class="mini_cart__remove"></span>
                                            <a class="mini_cart__image" href="#">
                                                <img src="images/cart_image_02.jpg" class="img-fluid" alt="">
                                            </a>
                                            <div class="mini_cart__content">
                                                <a class="mini_cart__name" href="#">Chaussons mules homme ultra légers</a>
                                                <div class="mini_cart__price">33,90 €</div>
                                            </div>
                                        </li>
                                    </ul>
                                    <div class="mini_cart__total">
                                        <span>TOTAL</span><strong>113,89 €</strong>
                                    </div>
                                    <div class="text-center">
                                        <a class="btn btn_sm btn_right" href="#"><span>Оформить покупки</span></a>
                                    </div>
                                </div>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>

</header>