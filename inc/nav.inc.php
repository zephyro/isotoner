<nav class="nav">
    <div class="nav__container">
        <ul class="nav__primary">
            <li class="dropdown">
                <a href="#">Женщинам<span></span></a>
                <div class="nav__second">
                    <div class="nav__second_row">
                        <ul class="nav__second_list">
                            <li>
                                <a href="#">
                                    <i><img src="img/nav/nav__icon_01.png" class="img-fluid" alt=""></i>
                                    <span>Slippers</span>
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    <i><img src="img/nav/nav__icon_05.png" class="img-fluid" alt=""></i>
                                    <span>Gloves</span>
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    <i><img src="img/nav/nav__icon_09.png" class="img-fluid" alt=""></i>
                                    <span>Umbrellas</span>
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    <i><img src="img/nav/nav__icon_02.png" class="img-fluid" alt=""></i>
                                    <span>Hats and Scarfs</span>
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    <i><img src="img/nav/nav__icon_06.png" class="img-fluid" alt=""></i>
                                    <span>Shoes</span>
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    <i><img src="img/nav/nav__icon_10.jpg" class="img-fluid" alt=""></i>
                                    <span>Bags</span>
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    <i><img src="img/nav/nav__icon_03.png" class="img-fluid" alt=""></i>
                                    <span>Rain hats</span>
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    <i><img src="img/nav/nav__icon_07.png" class="img-fluid" alt=""></i>
                                    <span>Sunglasses</span>
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    <i><img src="img/nav/nav__icon_11.png" class="img-fluid" alt=""></i>
                                    <span>Glasses</span>
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    <i><img src="img/nav/nav__icon_04.png" class="img-fluid" alt=""></i>
                                    <span>New</span>
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    <i><img src="img/nav/nav__icon_08.png" class="img-fluid" alt=""></i>
                                    <span>Exclusives</span>
                                </a>
                            </li>
                        </ul>
                        <div class="nav__second_image">
                            <a href="#">
                                <img src="images/nav__image_01.jpg" class="img-fluid" alt="">
                            </a>
                        </div>
                    </div>
                </div>
            </li>
            <li class="dropdown">
                <a href="#">Мужчинам<span></span></a>
                <div class="nav__second">
                    <div class="nav__second_row">
                        <ul class="nav__second_list">
                            <li>
                                <a href="#">
                                    <i><img src="img/nav/nav__icon_01.png" class="img-fluid" alt=""></i>
                                    <span>Slippers</span>
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    <i><img src="img/nav/nav__icon_05.png" class="img-fluid" alt=""></i>
                                    <span>Gloves</span>
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    <i><img src="img/nav/nav__icon_09.png" class="img-fluid" alt=""></i>
                                    <span>Umbrellas</span>
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    <i><img src="img/nav/nav__icon_02.png" class="img-fluid" alt=""></i>
                                    <span>Hats and Scarfs</span>
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    <i><img src="img/nav/nav__icon_06.png" class="img-fluid" alt=""></i>
                                    <span>Shoes</span>
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    <i><img src="img/nav/nav__icon_07.png" class="img-fluid" alt=""></i>
                                    <span>Sunglasses</span>
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    <i><img src="img/nav/nav__icon_11.png" class="img-fluid" alt=""></i>
                                    <span>Glasses</span>
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    <i><img src="img/nav/nav__icon_04.png" class="img-fluid" alt=""></i>
                                    <span>New</span>
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    <i><img src="img/nav/nav__icon_08.png" class="img-fluid" alt=""></i>
                                    <span>Exclusives</span>
                                </a>
                            </li>
                        </ul>
                        <div class="nav__second_image">
                            <a href="#">
                                <img src="images/nav__image_02.jpg" class="img-fluid" alt="">
                            </a>
                        </div>
                    </div>
                </div>
            </li>
            <li class="dropdown">
                <a href="#">Детям</a>
                <span></span>
                <div class="nav__second">
                    <div class="nav__second_row">
                        <ul class="nav__second_list">
                            <li>
                                <a href="#">
                                    <i><img src="img/nav/nav__icon_01.png" class="img-fluid" alt=""></i>
                                    <span>Slippers</span>
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    <i><img src="img/nav/nav__icon_05.png" class="img-fluid" alt=""></i>
                                    <span>Gloves</span>
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    <i><img src="img/nav/nav__icon_09.png" class="img-fluid" alt=""></i>
                                    <span>Umbrellas</span>
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    <i><img src="img/nav/nav__icon_02.png" class="img-fluid" alt=""></i>
                                    <span>Hats and Scarfs</span>
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    <i><img src="img/nav/nav__icon_06.png" class="img-fluid" alt=""></i>
                                    <span>Shoes</span>
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    <i><img src="img/nav/nav__icon_12.png" class="img-fluid" alt=""></i>
                                    <span>Indoor socks</span>
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    <i><img src="img/nav/nav__icon_07.png" class="img-fluid" alt=""></i>
                                    <span>Sunglasses</span>
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    <i><img src="img/nav/nav__icon_04.png" class="img-fluid" alt=""></i>
                                    <span>New</span>
                                </a>
                            </li>
                        </ul>
                        <div class="nav__second_image">
                            <a href="#">
                                <img src="images/nav__image_03.jpg" class="img-fluid" alt="">
                            </a>
                        </div>
                    </div>
                </div>
            </li>
            <li class="dropdown">
                <a href="#">Обувь<span></span></a>
                <div class="nav__second">
                    <ul class="nav__second_links">
                        <li>
                            <a href="#">
                                <i>
                                    <img src="images/nav__image_04.jpg" class="img-fluid" alt="">
                                </i>
                                <span>Женщинам</span>
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <i>
                                    <img src="images/nav__image_05.jpg" class="img-fluid" alt="">
                                </i>
                                <span>Мужчинам</span>
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <i>
                                    <img src="images/nav__image_06.jpg" class="img-fluid" alt="">
                                </i>
                                <span>Детям</span>
                            </a>
                        </li>
                    </ul>
                </div>
            </li>
            <li class="dropdown">
                <a href="#">Перчатки<span></span></a>
                <div class="nav__second">
                    <ul class="nav__second_links">
                        <li>
                            <a href="#">
                                <i>
                                    <img src="images/nav__image_07.jpg" class="img-fluid" alt="">
                                </i>
                                <span>Женщинам</span>
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <i>
                                    <img src="images/nav__image_08.jpg" class="img-fluid" alt="">
                                </i>
                                <span>Мужчинам</span>
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <i>
                                    <img src="images/nav__image_09.jpg" class="img-fluid" alt="">
                                </i>
                                <span>Детям</span>
                            </a>
                        </li>
                    </ul>
                </div>
            </li>
            <li class="dropdown">
                <a href="#">Зонты<span></span></a>
                <div class="nav__second">
                    <ul class="nav__second_links">
                        <li>
                            <a href="#">
                                <i>
                                    <img src="images/nav__image_10.jpg" class="img-fluid" alt="">
                                </i>
                                <span>Женщинам</span>
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <i>
                                    <img src="images/nav__image_11.JPG" class="img-fluid" alt="">
                                </i>
                                <span>Мужчинам</span>
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <i>
                                    <img src="images/nav__image_12.jpg" class="img-fluid" alt="">
                                </i>
                                <span>Детям</span>
                            </a>
                        </li>
                    </ul>
                </div>
            </li>
            <li class="dropdown">
                <a href="#">Зимняя коллекция<span></span></a>
                <div class="nav__second">
                    <ul class="nav__second_links">
                        <li>
                            <a href="#">
                                <i>
                                    <img src="images/nav__image_13.jpg" class="img-fluid" alt="">
                                </i>
                                <span>Женщинам</span>
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <i>
                                    <img src="images/nav__image_14.jpg" class="img-fluid" alt="">
                                </i>
                                <span>Мужчинам</span>
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <i>
                                    <img src="images/nav__image_15.jpg" class="img-fluid" alt="">
                                </i>
                                <span>Детям</span>
                            </a>
                        </li>
                    </ul>
                </div>
            </li>
            <li><a href="#">Распродажа</a></li>
        </ul>
    </div>
</nav>